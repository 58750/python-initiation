def print_hello():
    print("hello")


def addition(a, b):
    return a + b


def power(x, y=2):
    return x**y


def count_a(s: str):
    nb = s.count("a")
    return nb


def guess_nb():
    print("You have 10 tries to guess the secret number")
    tries = 10
    guess = int(input("input your first guess : "))
    while tries > 0:
        if guess == 13:
            print("Well played, you guessed the number !")
            return
        else:
            tries -= 1
            if tries == 0:
                print("You lost")
                return
            else:
                guess = int(input("Only " + str(tries) + " tries left. Enter a new guess : "))


print_hello()
sum1 = addition(2, 4)
print(sum1)

p1 = power(2)
p2 = power(4, 4)
p3 = power(y=3, x=4)
nb_of_a = count_a("I have a certain quantity of a")
