fp = open("data.txt")   # Open the file
data = fp.readlines()   # Stock lines of file inside a variable : data will be a list, each line being one item of it
print("file contains : {}".format(data))    # Another method of string formatting
fp.close()              # File has to be closed

with open("data.txt", "r") as fp:  # "r" means read. File is opened in read mode
    data = fp.read()            # Simple read : gets file content into a single string
    print(type(data))
    print("file contains : {}".format(data))

eco_mark = int(input("Enter the mark in economics : "))
math_mark = int(input("Enter the mark in math : "))
english_mark = int(input("Enter the mark in english : "))
marks = {"economics": eco_mark, "math": math_mark, "english": english_mark}
report_card = "report_card.txt"

# File open in write mode, "a" means append, meaning that things written by this code will be added at the end of file
with open("bulletin.txt", "a") as fp:
    for subject, mark in marks.items():
        fp.write("{} : {}\n".format(subject, mark))     # subject and mark written in the file, each time on a new line

mean = (eco_mark + math_mark + english_mark) / 3

with open(report_card, "a") as fp:
    fp.write("mean : {}\n".format(mean))
