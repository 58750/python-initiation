# Séance 5

## Les fonctions

Nous avons déjà utilisé plusieurs fonctions de base du langage Python. Dans cette partie, nous allons voir comment écrire nos propres fonctions.


Une fonction informatique peut recevoir quelque chose en entrée et peut renvoyer quelque chose en sortie.

Par exemple, la fonction `input()` prend en argument d'entrée une chaîne de caractère pour l'afficher dans la console et renvoie la chaîne de caractère rentrée dans la console par l'utilisateur·ice.

Une fonction peut ne prendre aucun argument en entrée. Également, une fonction peut ne "rien" renvoyer. En Python, si une fonction "ne renvoie rien", elle renvoie en pratique un objet de type `None`.

Voici la syntaxe pour écrire une fonction :

```python
def print_hello():
    print("hello")
```

Cette fonction ne possède aucun paramètre d'entrée, et "ne renvoie rien" (donc en réalité `None`).

NB : La convention de nommage pour les fonctions est la même que pour les variables : tout en minuscules, avec des `_` pour séparer les mots.

Essayez maintenant d'exécuter la fonction que vous venez d'écrire :

```python
print_hello()
```

La console affiche bien `bonjour`.

Écrivons maintenant une fonction effectuant une addition de deux nombres et qui renvoie le résultat :

```python3
def addition(a, b):
    return a + b
```

Cette fonction possède deux paramètres et renverra la somme de ces deux objets via le mot clef `return`.

Écrivez cette fonction en dessous de la première et ajoutez les lignes suivantes après `print_hello()` :

```python
print_hello()
sum1 = addition(2, 4)
```

Dans la première ligne, on passe les arguments `4` et `8` aux paramètres `a` et `b`.

NB : il ne faut pas confondre paramètre et argument. Un paramètre est le nom que l'on donne à la variable d'entrée lors de la définition de la fonction (ici, `a` et `b` sont les paramètres de la fonction `addition`). Les arguments sont les objets qui sont passés aux arguments de la fonction lors de l'appel de celle-ci (ici, l'argument `4` est passé au paramètre `a`, et l'argument `8` au paramètre `b`).

La console affiche `12`. L'objet renvoyé est bien référencé par la variable `sum1`

Il est possible de définir une valeur par défaut pour un paramètre. Exemple :

```python
def power(x, y=2):
    return x**y
```

Maintenant, appelons cette fonction :

```python
p1 = power(2)
p2 = power(4, 4)
```

La première ligne passe simplement l'argument `2` au paramètre `x`. Lors de l'exécution de la fonction, `y` prendra donc sa valeur par défaut, à savoir `2`.

La seconde ligne passe l'argument `2` au paramètre `x` et l'argument `4` au paramètre `y`. La valeur par défaut de `y` (`2`) est écrasée.

Si l'on ne connaît pas l'ordre des paramètres d'une fonction, il est possible de spécifier explicitement vers quel paramètre on passe tel ou tel argument :

```python
p3 = power(y=3, x=4)
```

Lors de la définition d'une fonction, il est également possible d'indiquer quel type on attend de recevoir pour un argument :

```python
def count_a(s: str):
    nb = s.count("a")
    return nb
```

Appelons cette fonction :

```python
nb_of_a = count_a("I have a certain quantity of a")
#nb_of_a = count_a(2)
```

Relancez votre programme et affichez `nb_of_a` pour vérifier que cela vaut `5`. Maintenant, dé-commentez la ligne en dessous. On constate que PyCharm souligne le `2`, car PyCharm détecte que la fonction `count_a()` est censé prendre un type `str` en argument, et non un `int`. Si vous relancez quand même votre script, une erreur va être levée.

Cet exemple va nous permettre d'aborder le concept de variable locale. La variable `nb` dans la fonction `count_a(s)` est une variable locale. Cela signifie qu'elle "n'existe" que dans la fonction où elle est déclarée.

À l'inverse, une variable déclarée dans le contexte (*scope* en anglais) global est appelée variable globale. Si vous déclarez une variable avant la définition de vos fonctions, par exemple :

```python
a = 0
```

Vous pourrez utiliser la variable `a` dans toutes vos fonctions.

**ATTENTION : Il est très peu recommandé d'utiliser des variables globales.**

Il est possible d'écrire plusieurs `return` dans une fonction. À partir du moment où un est atteint, l'exécution de la fonction se termine en renvoyant ce qui est placé après. On peut d'ailleurs utiliser un `return` sans rien renvoyer explicitement. Exemple :

```python
def guess_nb():
    print("You have 10 tries to guess the secret number")
    tries = 10
    guess = int(input("input your first guess : "))
    while tries > 0:
        if guess == 13:
            print("Well played, you guessed the number !")
            return
        else:
            tries -= 1
            if tries == 0:
                print("You lost")
                return
            else:
                guess = int(input("Only " + str(tries) + " tries left. Enter a new guess : "))
```

Les fonctions présentent un intérêt fondamental : éviter d'avoir plusieurs fois le même bout de code dans son programme. En informatique, on cherche toujours à "compartimenter" son code afin d'éviter les répétitions inutiles.


## Les modules

En Python, on appelle module un fichier Python. On peut voir un module comme une "boit à outils".

Python contient plusieurs modules natifs. Pour utiliser les fonctions d'un module dans un script, il faut d'abord l'importer :

```python
import math
from random import *
from datetime import date
```

La première ligne sert à importer l'intégralité du module `math`. La deuxième importe également toutes les fonctions du module `time`. La troisième ligne importe uniquement la fonction `date` du module `datetime`.

Voici comment utiliser les fonctions de ces modules :

```python
y = math.sqrt(100)
j = uniform(10, 100)
jour = date(day=4, month=12, year=2021)
print(jour)
```

NB : il vaut mieux éviter d'importer tout un module en faisant `from module import *`. En effet, si l'on utilise beaucoup de modules dans un programme, il peut y avoir des conflits de nommages, car certains modules peuvent avoir des fonctions avec le même nom. Privilégiez donc `import module`, afin d'écrire explicitement dans votre code le nom du module, suivi du nom de la fonction : `module.fonction()`.

Par convention, certains modules sont utilisés avec un alias. Par exemple :

```python
import random as rand

nb = rand.randint()
```

Nous avons expliqué plus tôt ce qu'est une méthode : une fonction opérant sur un objet (par exemple `liste.append()`). Un objet peut aussi "posséder" un attribut. Le terme attribut désigne un objet (référencé par une variable) appartenant à un objet.

Par exemple, pour récupérer le jour d'une date :

```python
d = date.datetime(2021, 12, 4) # d référence vers un objet de type date.datetime
jour = d.day # La variable day est un attribut de l'objet référencé par d. Elle référencera donc ici vers 4
```

Nous présenterons plus en détail les attributs dans la partie programmation orientée objet.

Il est possible de créer ses propres modules. Un module étant un simple fichier Python, "n'importe lequel" de vos fichiers peut être un module.

Comme pour les fonctions, compartimenter des fonctionnalités dans des modules permet de rendre son programme plus lisible, car mieux compartimenté, et de créer une "boite à outils" réutilisable dans d'autres projets.

### La condition `if __name__ == "__main__":`

Nous venons de dire que n'importe quel fichier Python peut être considéré comme un module, et donc importé dans un autre fichier Python.

Mais un fichier Python peut à la fois être considéré comme un module (importé dans un autre fichier) et comme un programme (exécuté).

La condition `if __name__ == "__main__":` permet de définir clairement du code à exécuter uniquement si le fichier Python est exécuté en tant que script.

Ci-dessous, deux fichiers (*demo_main1.py* et *demo_main2.py*) pour exemple :

```python
def randon_function():
    print("print inside the random function of demo_main1")


print("print outside the main of demo_main1")

if __name__ == "__main__":
    print("print inside the main of demo_main1")
```

```python
import demo_main1

if __name__ == "__main__":
    demo_main1.randon_function()
```

Si vous lancez *demo_main1*, vous aurez la sortie console suivante :

```
print outside the main of demo_main1
print inside the main of demo_main1
```

Si maintenant vous lancez *demo_main2*, vous aurez la sortie suivante :

```
print outside the main of demo_main1
print inside the random function of demo_main1
```

Lorsque lon exécute *demo_main1*, les deux messages sont affichés. Lorsqu'on importe *demo_main1* dans *demo_main2*, `print outside the main of demo_main1` est quand même affiché dans la console, mais pas `print inside the main of demo_main1`.

La condition `if __name__ == "__main__":` permet de définir du code qui ne sera exécuté que si le fichier est exécuté directement. On peut voir ça comme la "fonction" principale (*main* en anglais) du fichier. Il est recommandé de toujours écrire son programme principal dans cette condition `if`, pour éviter justement le comportement observé lors de l'exécution de *demo_main2*.

## La gestion de fichiers

Nous allons voir dans cette partie comment lire et écrire dans un fichier.

En effet, manipuler des fichiers en lecture et en écriture permet à la fois de travailler avec des données existantes, et de faire des sauvegardes "en dur".

En Python, il existe deux manières pour travailler avec des fichiers. Voyons la première, en mode lecture :

```python
fp = open("data.txt")   # Open the file
data = fp.readlines()   # Stock lines of file inside a variable : data will be a list, each line being one item of it
print("file contains : {}".format(data))    # Another method of string formatting
fp.close()              # File has to be closed
```

Évidemment, le fichier `data.txt` doit déjà exister dans le repertoire courant.

Voici la seconde méthode, plus "*pythonic*", à privilégier :

```python
with open("data.txt", "r") as fp:  # "r" means read. File is opened in read mode
    data = fp.read()            # Simple read : gets file content into a single string
    print(type(data))
    print("file contains : {}".format(data))
```

Voyons maintenant comment écrire dans un fichier :

```python
with open("bulletin.txt", "a") as fp:
    for subject, mark in marks.items():
        fp.write("{} : {}\n".format(subject, mark))     # subject and mark written in the file, each time on a new line

mean = (eco_mark + math_mark + english_mark) / 3

with open(report_card, "a") as fp:
    fp.write("mean : {}\n".format(mean))
```

Le second argument passé à la fonction `open()` correspond au mode d'accès.

`r` signifie lecture, `w` écriture (avec placement du curseur d'écriture au début du fichier, donc à éviter, car le contenu éventuellement existant dans le fichier sera effacé) et `a` pour *append* (le curseur est placé à la fin du fichier, donc ce qui y sera écrit sera placé à la suite du contenu déjà existant). Vous pouvez ajouter un `+` à la suite de la première lettre pour accéder à la fois en lecture et en écriture au fichier.

En mode écriture, si le fichier n'existe pas, il sera automatiquement créé. Lorsque vous allez lancer pour la première fois ce script, le fichier `report_card.txt` sera ainsi créé dans le répertoire.

## Exercices

### Le cycle jour/nuit

L'objectif de cet exercice est de simuler le cycle jour/nuit pour un jour donné.

Afin de simplifier l'exercice, le programme pourra afficher quatre cycles différents : un pour chaque saison.

- Été (juin, juillet, août) : le soleil se lève à 7 h et se couche à 21 h
- Automne (septembre, octobre, novembre) : le soleil se lève à 9 h et se couche à 19 h
- Hiver (décembre, janvier, février) : le soleil se lève à 10 h et se couche à 17 h
- Printemps (mars, avril, mai) : le soleil se lève à 8 h et se couche à 20 h

Pour simuler le cycle jour nuit dans la console, il faudra introduire un délai. Pour cela, il faut utiliser la fonction `sleep(secondes)` du module `time` qui permet de "bloquer" le programme pendant "x" secondes.

La date du jour doit être rentré dans la console par l'utilisateur·ice. Il faudra à partir de ses valeurs créer un objet de type `datetime.date`.

### Gestionnaire de comptes v2

[Énoncé de la v1 dans la séance 4](../Seance04/Seance04.md#gdcv1)

A priori, votre première version du gestionnaire de *login* devrait comporter plusieurs fois les mêmes portions de code. Essayez maintenant d'écrire une v2 écrivant et utilisant vos propres fonctions.

Comment démarrer :

```python
def is_username(logins: dict, username: str):
    pass
```

NB : l'instruction `pass` représente une instruction qui "ne fait rien".

Écrivez votre programme principal dans une condition `if __name__ == "__main__":`.

Ici, la fonction n'a pas encore de corps (= de code à l'intérieur). Pour quand même laisser cette fonction dans le script et pouvoir l'exécuter, on utilise l'instruction `pass`

Ensuite, ajoutez une fonctionnalité de sauvegarde des comptes dans un fichier texte. Votre programme devra :

- Au démarrage, ouvrir le fichier texte contenant les infos des comptes déjà existants
- À la fermeture, sauvegarder tous les nouveaux comptes créés dans ce même fichier
