# Séance 2

## Les structures conditionnelles : `if`, `elif`, `else`

La plupart du temps, lorsque l'on écrit un programme, celui-ci doit se comporter différemment en fonction de différentes conditions. C'est là qu'interviennent les structures conditionnelles.

Créez un nouveau fichier Python, appelé "demo_if.py". Ajoutez les lignes suivantes :

```python
a = 5
if a == 5:
    print("a is equal to 5")
```

Lancez ce script. La console affiche : `a est égal à 5 !`

Voici notre première structure conditionnelle, très simple. La première ligne affecte à la variable `a` la valeur 5
En dessous se trouve la structure en elle-même. Le mot clef `if` ("si" en français) marque le début d'un bloc d'instruction. Le symbole `:` est également obligatoire pour marquer le début d'un bloc.

En effet, si a est égal à 5 (condition que l'on vérifie en utilisant l'opérateur `==`), alors l'opération renvoie `True`, et le code placé dans ce bloc sera exécuté. Soit ici, la ligne avec la fonction `print()`.

En python, pour placer des instructions dans un bloc, il faut indenter la ligne (avec 4 espaces ou une tabulation). Vous voyez qu'ici, le `print()` n'est pas au même niveau que les lignes du dessus, mais décalé "une fois" vers la droite.

Changez la valeur de `a` (n'importe laquelle sauf 5 donc) dans votre code et ajoutez la ligne suivante en dessous, sans l'indenter :

```python
print("print outside the if bloc")
```

La console affichera uniquement le message `print en dehors du bloc`. En effet, cette fois, la condition `a == 5` n'est pas vérifiée. L'opération renvoie donc `False`, et l'instruction `print("a est égal à 5")` n'est pas exécutée.

Par contre, la ligne suivante, qui ne fait pas partie de la structure conditionnelle, est bien exécutée.

Pour réaliser des structures conditionnelles avec plusieurs "chemins" possibles, on utilise les mots clefs `elif` (pour *else if*, "sinon si" en français) et `else` ("sinon" en français).

Ajoutez ce code à la suite de votre script : 

```python
b = 10
if b == 10:
    print("b is 10")
elif b == 15:
    print("b is 15")
else:
    print("b is neither 10 nor 15")
```

La console affiche `b vaut 10`. Testez le programme en changeant la valeur de `b`.

En mettant `b = 15`, alors c'est la deuxième condition (celle du `elif`) qui est vérifiée. Le message affiché sera donc `b vaut 15`. Si vous fixez `b` à n'importe quelle autre valeur, alors c'est l'instruction du bloc `else` qui est exécutée.

Maintenant, ajoutez le code suivant : 

```python
c = 20
if c > 10:
    print("c est strictly less to 10")
if c > 5:
    print("c est strictly greater to 5")
```

Ce bout de code va faire afficher `c est strictement supérieur à 10`, mais pas `c est strictement supérieur à 5`. Car dans une structure conditionnelle `if..elif..else`, l'ordre à son importance. Si la première condition est vérifiée, alors toutes celles qui viennent ensuite ne sont pas analysées et les instructions des blocs suivants ne seront pas exécutées.

Une structure conditionnelle commence donc toujours par un bloc `if`, suivi de zéro, un ou plusieurs bloc(s) `elif` pour se terminer (mais pas obligatoirement) par un bloc `else`.

Si on remplace le `elif` par un `if` dans le bout de code ci-dessus, alors on obtient deux structures conditionnelles indépendantes. La console affichera cette fois : 

```
c est strictement supérieur à 10
c est strictement supérieur à 5
```

Notez enfin, qu'il est tout à fait possible d'imbriquer un bloc de code dans un autre. Il est donc possible d'avoir une structure conditionnelle dans un bloc d'une structure. Exemple :

```python
d = 12
if d > 5:
    print("d > 5")
    if d > 10:
        print("d > 10")
```

On repère facilement les différents "niveaux" dans le code grâce à l'indentation. Ici, si `a` est supérieur à 10, les deux `print()` seront affichés dans la console. Par contre, si `a` est compris entre 5 (exclus) et 10 (exclus), la console n'affichera que `a > 5`.

## La boucle `while`

Dans la plupart des programmes informatiques, il est nécessaire d'effectuer plusieurs fois la même action, donc effectuer quelque chose "en boucle". On parle également d'itérations, et donc de boucle itérative.

Une des deux manières de faire en Python est la boucle `while` ("tant que" en français). Comme son nom le laisse supposer, elle sert à effectuer une action tant que sa condition d'entrée est `True`.

Une boucle `while`, comme pour les `if`, `elif` et `else`, s'écrit en tant que bloc d'instructions :

```python
n = 0
while n < 10:
    print("n = " + str(n))
    n += 1  # Assignment and modification in 1 instruction : equivalent to n = n + 1
print("Program exited the while loop")
```

Créez un nouveau script Python appelé "demo_while.py". Écrivez-y le code ci-dessus et exécutez-le. Vous devriez obtenir la chose suivante dans la console :

```
n = 0
n = 1
n = 2
n = 3
n = 4
n = 5
n = 6
n = 7
n = 8
n = 9
Programme sorti de la boucle while
```

Les instructions à l'intérieur de la boucle ont été exécutées 10 fois. En effet, lorsque l'interpréteur arrive pour la première fois à la ligne 2 du code, la condition `n < 10` est `True` (car `n` vaut 0). Le programme "rentre" dans la boucle et exécute les deux lignes.

NB : la deuxième ligne de la boucle contient un opérateur d'affectation particulier : il sert à "mettre à jour" la valeur référencée par la variable `n`. À la fin de cette première itération, `n` vaut 1.

Là, le programme retourne au début de la boucle et vérifie à nouveau si la condition est `True`. C'est toujours le cas, donc les instructions de la boucle sont à nouveau exécutées. Et ainsi de suite, 10 fois.

À la fin de la dixième itération, `n` vaut 10. Par conséquent, quand le programme retourne vérifier la condition d'entrée de la boucle, celle-ci renvoie `False`, car `n` n'est pas strictement inférieur à 10. Le programme ne rentre donc pas dans la boucle et va continuer en exécutant l'instruction suivante, en l'occurrence `print("Programme sorti de la boucle while)`.


## Envoyer des informations à un programme via la console

Pour l'instant, nous avons simplement vu comment faire en sorte que le programme "nous envoie" des informations, via la fonction `print()`. Mais pour que la communication puisse se faire dans les deux sens, il faut faire en sorte que l'utilisateur·ice du programme puisse envoyer des informations au programme.

En Python il suffit d'utiliser la fonction `input()`. Exemple :

```python
name = input("Enter your name : ")
print("Hello " + name)
```

La première ligne est une affectation. En effet, on a un signe `=`, une variable `name` à sa gauche et la fonction `input()` à sa droite. La fonction `input()` renvoie un objet de type `str`, qui contiendra ce qui aura été entré au clavier dans la console.
Exécutez ce programme (dans un fichier Python quelconque). Vous verrez dans la console que la console "est en attente". On dit que la fonction `input()` est une fonction bloquante, car elle attend que quelque chose de spécifique se passe, et donc interrompt l'exécution du programme en attendant.

Si vous cliquez dans la console, vous verrez un curseur apparaître. Tapez quelque chose et appuyez sur Entrée.

La fonction `input()` a bien "lu" votre nom, et l'a stocké dans la variable `name`. Votre nom réapparaît ensuite dans la ligne du dessous grâce à la fonction `print()`. L'exécution du script s'est bien terminé.

## Le débugueur

Un débugueur est un outil permettant de déboguer son programme. Déboguer signifie chercher et corriger les erreurs de son programme.

En Python, un débogueur permet de tester son programme étape par étape, afin de voir le "chemin" qui suit l'interpréteur et l'évolution de ses variables. Pour déboguer, il faut cliquer sur le petit insecte vert à côté du bouton "run" en haut à droite.

Testez par exemple dans votre fichier "demo_while.py". En l'état, il n'y aura aucune différence avec un simple *run*. Pour tirer parti des fonctionnalités, il faut dans un premier temps ajouter au moins un *breakpoint*. Pour ajouter un *breakpoint* dans un fichier, cliquez à gauche d'une ligne de code. Testez avec la première du fichier.

Un cercle rouge vous indique qu'un *breakpoint* se trouve maintenant à cette ligne. Relancez le *debug*. Cette fois l'onglet "Debug" s'ouvrira en bas à la place de la sortie console. En effet l'interpréteur s'est arrêté au premier *breakpoint* du fichier, donc à la première instruction.

En haut de la fenêtre de debug se trouvent plusieurs icônes bleues avec des flèches. Le bouton *Step Into* permet d'exécuter les instructions une par une, et ainsi de voir le "chemin" suivi par le programme.

Vous pourrez voir à droite de la fenêtre de debug l'évolution des variables, en l’occurrence ici la variable `n`. De même, dans l'éditeur de texte, vous pouvez voir que l'interpréteur répète bien la boucle `while` tant que `n` est strictement inférieur à 10.

Si vous appuyez sur le bouton "Resume" à gauche de la fenêtre de debug, alors le débugueur ira jusqu'au prochain *breakpoint*. Ici, comme vous n'en avez pas placé d'autres, le programme sera exécuté jusqu'à la fin.

Utiliser un débugueur permet ainsi d'analyser en profondeur le fonctionnement de son programme, pour mieux détecter les erreurs qui peuvent s'y trouver. Cela peut aussi servir d'outil pour mieux comprendre le fonctionnement des structures conditionnelles ou des boucles.

## Exercices

### Thermomètre approximatif

Écrivez un programme qui affichera un ou deux messages en fonction d'une température entrée par l'utilisateur·ice. Votre programme devra commencer par la ligne suivante :

```python
temp = int(input("Enter the temperature : "))
```

Votre programme devra forcément afficher l'une des trois phrases suivantes :

- Si la température est négative : "Temperature is positive"
- Si la température est positive : "temperature is negative"
- Si la température est de 0 °C : "Temperature is null"

Et devra, si :
- La température est supérieure à 40 °C, afficher : "It's really hot !"
- La température est en dessous de -30 °C, afficher : "It's really cold !"

### Tarif d'une place de cinéma

Écrivez un programme fixant le tarif d'une place de cinéma. Le prix de la place dépendra de l'âge de la personne et du fait qu'elle possède une carte de réduction ou non.

3 tarifs de base possibles : 

- Tarif normal : 10 €
- Tarif moins de 6 ans : 3 €
- Tarif entre 6 et 17 ans (inclus) et plus de 65 ans (inclus) : 5 €

Et si la personne possède une carte de réduction, le prix de son billet est réduit de 50%.

Utilisez la fonction `input()` pour demander l'âge de la personne et si elle possède une carte de réduction. Puis déduisez de ces deux informations le prix de la place.

### Racines d'un polynôme du second degré

Écrivez un programme permettant de calculer les racines d'un polynôme du second degré, ax² + bx + c$

Le programme demandera les trois coefficients a, b et c, puis affichera en fonction du discriminant Delta s'il y a deux racines réelles, une racine double réelle ou deux racines complexes.

Indice : allez voir la documentation du type *complex* natif au Python.

### Le juste prix

Écrivez un programme reprenant le principe du juste prix, à savoir trouver le prix exact d'un produit quelconque.

Le programme demandera un prix, et répondra en disant si le prix à trouver est plus grand ou plus petit que le prix entré. Le programme continuera de demander une proposition tant que vous n'avez pas donné le juste prix.

Écrivez ces deux lignes au début de votre programme :

```python
prix = 450  # Price to guess, try with different values to test your program
proposition = int(input("Enter a guess for the price : "))
```

Testez pour différentes valeurs de `prix`, afin de vérifier que votre algorithme fonctionne.

### Dessiner un sapin

Écrivez un programme qui dessinera un sapin comme ci-dessous :

```
    *    
   ***
  *****
 *******
*********
    |
```

Avant de l'afficher, le programme demandera à l'utilisateur·ice le nombre "d'étages" que le sapin devra avoir.