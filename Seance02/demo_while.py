n = 0
while n < 10:
    print("n = " + str(n))
    n += 1  # Affectation and modification in 1 instruction : equivalent to n = n + 1
print("Program exited the while loop")

nom = input("Enter your name : ")
print("Hello " + nom)
