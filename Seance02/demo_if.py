a = 5
if a == 5:
    print("a is equal to 5")

print("print outside the if bloc")

b = 10
if b == 10:
    print("b is 10")
elif b == 15:
    print("b is 15")
else:
    print("b is neither 10 nor 15")

c = 20
if c > 10:
    print("c est strictly less to 10")
if c > 5:
    print("c est strictly greater to 5")

d = 12
if d > 5:
    print("d > 5")
    if d > 10:
        print("d > 10")
