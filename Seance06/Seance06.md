# Séance 6

## La Programmation Orientée Objet : introduction

POO : Programmation Orientée Objet

La POO est un paradigme : c'est-à dire une "manière" d'appréhender le fonctionnement de son programme. Jusqu'ici, vos programmes fonctionnaient de manière structurée : fonctionnement séquentiel, conditionnel, répétitif, avec des fonctions.

La POO, comme son nom l'indique, consiste à considérer son programme non pas comme un ensemble de fonctions, mais comme un ensemble d'objets. Un objet possède des caractéristiques, des états et des comportements.

Nous avons déjà dit qu'en Python, **tout est objet**. Un `int`, un `float`, une `list`... sont des objets. L'intérêt de la POO, c'est de pouvoir définir ses propres objets, grâce aux classes.

### Les classes

Une classe, c'est le "plan" (*blueprint* en anglais) d'un objet. Cela énumère les caractéristiques et fonctionnalités qu'un objet issu de cette classe va posséder.

On peut faire l'analogie avec un patron en couture. Un patron vous donne les dimensions des différents morceaux de tissus, où coudre, plier... pour créer votre vêtement. Mais ce n'est pas le vêtement en lui-même. Et à partir d'un même patron, vous pouvez créer plusieurs fois le même vêtement, tout en y ajoutant à chaque fois des variations : il pourra être bleu et en coton, rouge et en acrylique, vert et en lin...

La classe, c'est le patron du pantalon et l'objet, c'est le pantalon noir en coton.

Formulé autrement, on dira que le pantalon noir en coton est une instance de la classe pantalon.

Et on peut aussi dire que l'objet pantalon noir en coton est du type pantalon.

Les termes classe et type désignent en fait la même chose, idem pour les termes objets et instance. En pratique on utilisera les uns ou les autres en fonctions du contexte.

En POO, les caractéristiques sont appelées **attributs**. Toujours pour notre classe pantalon, on aura l'attribut couleur et l'attribut matière.

Les comportements d'un objet sont ses fonctionnalités, donc des fonctions. Les fonctions d'une classe sont appelées **méthodes**. Pour un pantalon, on peut imaginer par exemple les méthodes ouvrir la braguette, fermer la braguette, ranger dans poche droite...

Voici comment écrire cette classe en Python :

```python
class Trouser:

    def __init__(self, color, material):
        self.color = color
        self.material = material
        self.zip_opened = False

    def __str__(self):
        return f"The trouser is {self.color} and is made of {self.material}. " \
               f"Is its zip opened : {self.zip_opened}"

    def open_zip(self):
        self.zip_opened = True

    def close_zip(self):
        self.zip_opened = False
```

Le mot clef `class` permet de définir le nom de la classe. NB : la convention de nommage des classes n'est pas la même que pour les variables ou les fonctions. Tous les mots doivent commencer par une majuscule, sans êtres séparés par un `_`.

Cette classe contient quatre méthodes :

- `__init__(self, color, material)` : méthode **constructeur**. C'est cette méthode qui est exécutée lorsque l'on souhaite créer un nouvel objet de type `Trouser`.
- `__str__(self)` : méthode pour le `print`. Renvoie une chaine de caractères, qui sera affiché lorsque l'on fait `print(trouser_name)`.
- open_zip(self) : modifie l'attribut de l'objet, pour "ouvrir la braguette"
- close_zip(self) : pour fermer la braguette.

### Instancier une classe : créer un objet

Ajoutons un `main` à notre script pour créer des instances de cette classe :

```python
if __name__ == "__main__":
    blue_denim = Trouser("blue", "denim")
    brown_chino = Trouser("brown", "chino")
    print(brown_chino.color)  # Access value of attribute material of the object referenced by brown_chino variable
    print(brown_chino)
    blue_denim.open_zip()   # Executes open_zip() method of object referenced by blue_denim variable
    print(blue_denim)
```

Les variables `blue_denim` et `brown_chino` sont deux variables référençant vers deux objets différents, ayant des caractéristiques différentes, mais qui sont tous deux de type `Trouser`.

Comme expliqué plus haut, ces deux commandes vont donc appeler la méthode `__init__()` de la classe. Cette méthode sert à créer l'objet, et à initialiser les valeurs des attributs, qui sont dans cet exemple :

- `color`, pour représenter la couleur du pantalon
- `material`, pour représenter la matière
- `opened_zip`, pour représenter l'état de la fermeture Éclair (fermé ou ouvert)

La troisième instruction affichera `brown`, soit la valeur de l'attribut `color`.

La quatrième ligne exécute la méthode `__str__()`.

La cinquième ligne appelle la méthode `open_zip()` sur l'objet `blue_denim`. On voit bien grâce au `print` de la sixième ligne que la valeur de l'attribut `opened_zip` de `blue_denim` a été modifiée.

### Le paramètre `self`

Le paramètre `self` est un paramètre à ajouter dans la définition des méthodes de classes, pour accéder aux membres de la classe elle-même dans celle-ci.

Dans le constructeur `__init__()`, on utilise `self` pour définir les attributs de la classe. Dans les autres méthodes de la classe, `self` permet d'accéder à ces mêmes attributs.

Ainsi, les deux instructions ci-dessous font la même chose : appeler la méthode `open_zip` "sur" l'objet référencé par `blue_denim`.

```python
blue_denim.open_zip()
Trouser.open_zip(blue_denim)
```

## Exercice

### Combat au tour par tour

Créez un programme qui va faire combattre au tour par tour deux personnages. Il faudra donc définir une classe `Character`.

Un personnage doit posséder trois attributs :
- `Health` : compris entre 15 et 20
- `Defense` : compris entre 5 et 7
- `Strength` : compris entre 10 et 12

Ces trois valeurs doivent être fixées au hasard et aléatoirement lors de la création du personnage.

En plus des trois attributs, un personnage devra également avoir une méthode `attack()` pour infliger les dégâts. Les points de vie à retirer au personnage qui subit l'attaque se calcule en faisant la soustraction force moins défense.

NB : vous pouvez utiliser le module `time` pour mettre des pauses entre chaque attaque (cf. [Séance 5](../Seance05/Seance05.md))

### Bataille navale

Créez un programme qui permet de jouer à deux à la bataille navale.

Chaque joueur·euse possède cinq bateaux qu'il doit placer sur une grille de 10x10 cases. Les bateaux sont caractérisés par leur longueur :

| Nom        | taille | 
|------------|--------|
| Carrier    | 5      |
| Battleship | 4      |
| Cruiser    | 3      |
| Submarine  | 3      |
| Destroyer  | 2      |

Pour réaliser ce programme vous aller devoir définir plusieurs classes : une classe `Player`, une classe `Ship`, une classe `Grid`.

Un·e joueur·euse possède 2 grilles, celle où il place ses bateaux, et celle où il note les tirs envoyés chez l'adversaire.

Le jeu se déroule ainsi :

- Chaque joueur·euse place ses bateaux sur sa grille.
- Les joueur·euse·s tirent chacun leur tour sur l'adversaire, en désignant une case de la grille. Le programme doit indiquer si c'est "raté", "touché" ou "coulé" (bateau complétement détruit).
- Le résultat est noté sur la grille "adversaire".
- La partie est gagnée lorsque tous les bateaux d'un·e joueur·euse sont coulés.

[Plus de détails sur les règles du jeu sur sa page Wikipédia](https://fr.wikipedia.org/wiki/Bataille_navale_\(jeu\))