class Trouser:

    def __init__(self, color, material):
        self.color = color
        self.material = material
        self.zip_opened = False

    def __str__(self):
        return f"The trouser is {self.color} and is made of {self.material}. " \
               f"Is its zip opened : {self.zip_opened}"

    def open_zip(self):
        self.zip_opened = True

    def close_zip(self):
        self.zip_opened = False


if __name__ == "__main__":
    blue_denim = Trouser("blue", "denim")
    brown_chino = Trouser("brown", "chino")
    print(brown_chino.color)  # Access value of attribute material of the object referenced by brown_chino variable
    print(brown_chino)
    blue_denim.open_zip()   # Executes open_zip() method of object referenced by blue_denim variable
    Trouser.open_zip(blue_denim)    # Executes the same thing as instruction above
    print(blue_denim)
