class Trouser:

    nb_of_pants = 0     # class attribute

    def __init__(self, color, material):
        self.color = color
        self.material = material
        self.__class__.nb_of_pants += 1     # Access to class attribute via self

    @classmethod
    def print_nb_of_pants(cls):
        print(f"{cls.nb_of_pants} have been created as of now.")

    @classmethod
    def bluejean(cls):
        return cls(['blue', 'denim'])

    @staticmethod
    def cm_to_inches(cm):
        return cm*0.3937008
