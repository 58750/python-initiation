class Trouser:

    def __init__(self, color=0x000000):
        self._color = color

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, val):
        if val > 0XFFFFFF or val < 0:
            raise ValueError("Color must be set between 0X000000 and 0xFFFFFF")

        print(f"The trouser has been dyed : {hex(val)}")
        self._color = val


if __name__ == "__main__":

    trouser = Trouser()
    print(trouser.color)
    trouser.color = 0xFFFFFF
    print(trouser.color)
