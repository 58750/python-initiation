# Séance 9

## Les décorateurs

En Python, les fonctions sont, comme tout le reste, des objets. On peut donc affecter une fonction à une variable, comme on le ferait avec n'importe quel autre type

```python
def yell(sentence):
    return sentence.upper() + " !"


meow = yell
print(meow('meow'))
```

La variable `meow` référence maintenant vers la fonction définie au-dessus. Il est donc possible d'exécuter le code de la fonction via `yell` et `meow`.

Le fait de pouvoir avoir une variable référençant vers une fonction permet de pouvoir passer une fonction en argument d'une autre fonction.

```python
def greet(func):
    greeting = func("Hello, I'm a student")
    print(greeting)

greet(yell)
```

Si on exécute ce code, on obtient en sortie :

```
HELLO, I'M A STUDENT !
```

Car dans la fonction `greet()`, le paramètre `func` reçoit en argument la fonction `yell`. On peut ensuite appeler cette fonction à l'intérieur de `greet()`

```python
def whisper(sentence):
    return sentence.lower() + "."

greet(whisper)
```

De la même manière, le code ci-dessus donnera en sortie :

```
hello, i'm a student.
```

Il est également possible de définir des fonctions dans des fonctions, et de retourner des fonctions :

```python
def get_volume_func(volume):
    def whisper(sentence):
        return sentence.lower() + "."
    
    def yell(sentence):
        return sentence.upper() + " !"
    if volume > 0.5:
        return yell
    else:
        return whisper

chosen_volume = get_volume_func(0.3)
print(chosen_volume("How are you"))
```

Ici, `chosen_volume` va donc faire référence vers la fonction `whisper`.

On peut réécrire une fonction qui aura le même comportement :

```python
def get_volume_func_bis(sentence, volume):
    def whisper():
        return sentence.lower() + "."
    
    def yell():
        return sentence.upper() + " !"
    if volume > 0.5:
        return yell
    else:
        return whisper
```

L'intérêt de cette syntaxe, c'est que les fonctions `whisper` et `yell` peuvent accéder au paramètre `sentence` de la fonction parent.

En pratique, ce principe d'imbriquer des fonctions ne permet pas seulement de renvoyer des fonctions par des fonctions, mais surtout de préconfigurer le comportement des fonctions. Autre exemple :

```python
def create_adder(a):
    def add(b):
        return a + b
    return add

x_plus_3 = create_adder(3)
print(x_plus_3(6))
```

La console affichera `9`, car `x_plus_3` fait référence vers une fonction `create_adder()` dans laquelle le paramètre `a` vaut `3`.

C'est sur ce principe que repose les décorateurs. Par exemple :

```python
def uppercase(func):
    def wrapper():
        original_output = func()
        modified_output = original_output.upper()
        return modified_output
    return wrapper

@uppercase
def say_goodbye():
    return "goodbye"

print(say_goodbye())
```

Le décorateur `@uppercase` placé au-dessus de la fonction `say_goodbye()` vient ainsi modifier son comportement. La console va en effet afficher

```
GOODBYE
```

Il est évidemment possible d'écrire des décorateurs et donc des fonctions *wrapper* qui prennent des arguments. Il faut pour cela utiliser les paramètres `*args` et `**kwargs**`, que nous n'aborderons pas dans ce cours.

Ce qu'il est important de savoir, c'est qu'il existe des décorateurs permettant de modifier "pour vous" vos fonctions.

Par exemple, le décorateur `@property` qui est inclus dans Python de base, permet de définir plus "proprement" des *getter* et des *setter* :

```python
class Trouser:

    def __init__(self, color = 0x000000):
        self._color = color

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, val):
        if val > 0XFFFFFF or val < 0:
            raise ValueError("Color must be set between 0X000000 and 0xFFFFFF")

        print(f"The trouser has been dyed : {val}")
        self._color = val

        
if __name__ == "__main__":

    trouser = Trouser()
    print(trouser.color)
    trouser.color = 0xFFFFFF
    print(trouser.color)
```

L'attribut `_color` est bel et bien protégé, mais l'accès et la modification de l'attribut d'une instance de `Trouser` se fait de manière transparente, comme si celui-ci était public.

Les deux sections suivantes vont reposer sur l'utilisation d'autres décorateurs inclus de base dans Python.

## L'abstraction

L'abstraction est un concept de la POO permettant de définir des classes et des méthodes abstraites. Une classe abstraite, c'est une classe dont un ne peut pas instancier d'objet de son type.

Une méthode abstraite est une méthode qui ne contient pas de code, mais définit juste un nom, donc quelque part un concept de fonctionnalité. Exemple :

```python
from abc import ABC, abstractmethod

class Shape(metaclass=ABC):

    @abstractmethod
    def area(self):
        pass

    @abstractmethod
    def perimeter(self):
        pass


class Rectangle(Shape):

    def __init__(self, length, width):
        self.length = length
        self.width = width
    
    def area(self):
        return self.length * self.width
    
    def perimeter(self):
        return 2*self.length + 2*self.width

if __name__ == "__main__":

    s = Shape() # Will generate an error
    square = Rectangle(4, 4)
    print(square.area())
    print(square.perimeter())
```

Si vous essayez de créer une instance de `Shape`, la console vous affichera une erreur, car `Shape` hérite de la classe `ABC`.

Le décorateur `@abstractmethod` permet de définir une méthode abstraite. L'intérêt de définir une méthode abstraite est double. Cela permet de définir un concept "général" à l'intérieur d'une classe, sans pouvoir le représenter concrètement (avec du code). Et le fait d'ajouter le décorateur forcera les classes filles de redéfinir cette méthode abstraite.

Si vous effacez `perimeter()` de la classe `Rectangle, alors la ligne `square = Rectangle(4, 4)` générera aussi une erreur.

Le concept de classe abstraite n'est pas forcément beaucoup utilisé en Python, mais on peut le retrouver dans d'autres langages de programmation.

## Les membres de classe et statique

Jusqu'à présent, nous avons dans nos classes uniquement travaillées avec des membres (attributs et méthodes) d'instance. Mais il existe aussi des méthodes et attributs de classe et des méthodes statiques. Exemple :

```python
class Trouser:

    nb_of_pants = 0     # class attribute

    def __init__(self, color, material):
        self.color = color
        self.material = material
        self.__class__.nb_of_pants += 1     # Access to class attribute via self
        
    @classmethod
    def print_nb_of_pants(cls):
        print(f"{cls.nb_of_pants} have been created as of now.")

    @classmethod
    def bluejean(cls):
        return cls(['blue', 'denim'])

    @staticmethod
    def cm_to_inches(cm):
        return cm*0.3937008
```

`nb_of_pants` est un attribut de classe. C'est un attribut qui n'existe qu'une fois pour toute la classe, là où un attribut d'instance existe pour chaque instance (chaque objet créé à partir de la classe).

On peut accéder à un attribut de classe dans une méthode d'instance (ici `__init__(self)` en est une), mais il ne faut pas oublier le `__class__` après le `self`.

Pour définir une méthode comme étant une méthode de classe, on utilise le décorateur `@classmethod`. Une méthode de classe n'a pas (par convention) `self` mais `cls` comme paramètre d'entrée, pour bien indiquer que la méthode va agir sur la classe et pas sur une instance de cette classe.

Ainsi, on voit dans `print_nb_of_pants` qu'on peut directement accéder à l'attribut de classe en écrivant `cls.nb_of_pants`.

Comme une méthode de classe ne travaille pas sur une instance, elle ne peut pas accéder à des attributs d'instances.

En pratique, on utilise souvent des méthodes de classe pour créer des *factory functions*. Par exemple, ici, la méthode de classe `bluejean` permet de créer automatique un jean bleu.

La dernière méthode, décoré par `@staticmethod`, est une méthode statique. C'est une méthode qui ne peut agir ni sur les instances, ni sur la classe (pas de paramètre `self` ou `cls`). Par conséquent, écrire des méthodes statiques révèle plus de la manière dont on souhaite compartimenter son code.

S'il y a une action particulière (ici convertir des centimètres en pouces) que vous devez souvent faire quand vous travailler avec des instances d'une classe, cela peut être judicieux d'écrire cette fonction en tant que méthode statique pour en quelque sorte "l'avoir sous la main".

Là encore, ce n'est pas forcément des concepts qu'on retrouve tout le temps en Python, mais qui sont quasiment systématiquement utilisés dans d'autres langages orientés objet.
