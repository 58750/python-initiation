def yell(sentence):
    return sentence.upper() + " !"


def greet(func):
    greeting = func("Hello, I'm a student")
    print(greeting)


def whisper(sentence):
    return sentence.lower() + "."


def get_volume_func(volume):
    def whisper(sentence):
        return sentence.lower() + "."

    def yell(sentence):
        return sentence.upper() + " !"
    if volume > 0.5:
        return yell
    else:
        return whisper


def get_volume_func_bis(sentence, volume):
    def whisper():
        return sentence.lower() + "."

    def yell():
        return sentence.upper() + " !"
    if volume > 0.5:
        return yell
    else:
        return whisper


def create_adder(a):
    def add(b):
        return a + b
    return add


def uppercase(func):
    def wrapper():
        original_output = func()
        modified_output = original_output.upper()
        return modified_output
    return wrapper


@uppercase
def say_goodbye():
    return "goodbye"


meow = yell

print(meow('woof'))

greet(yell)

greet(whisper)

chosen_volume = get_volume_func(0.3)
print(chosen_volume("How are you"))

x_plus_3 = create_adder(3)
print(x_plus_3(6))

print(say_goodbye())