# Séance 3

## Les séquences

Le terme séquence désigne les types composites (ou *collection*) étant des "regroupements" de données ordonnées, (comme une série en maths). Chaque élément d'une séquence possède un indice, qui correspond à sa position dans la séquence.

Python propose 4 types de séquences : `str`, `list`, `tuple` et `range`.

`str` est bien une séquence. Nous avions vu dans la séance 1 qu'il était possible de récupérer seulement un caractère d'une chaîne. Le principe sera le même pour les autres types de séquence.

### Le type `list`

En Python, une liste est une séquence ordonnée modifiable (mutable) d'objets. On utilise les crochets `[]` pour créer une liste :

```python
l1 = [1, 2, 3.5, "hello", True, "red", 3.5, 5.5445, "b"]
```

Une liste peut contenir différents types d'objets. Ici, `l1` contient aussi bien un `int`, un `float`, un `str` ou un `bool`.

Pour accéder aux éléments d'une liste, il faut écrire la variable référençant vers cette liste, suivi de l'**indice** (la position) entre crochets. 

**RAPPEL** : on commence à "compter" les éléments à partir de 0 !

Exemples :

```python
a = l1[0]       # Get the first item of the list
b = l1[2:7]     # Get items from 3rd to 7th
c = l1[4:]      # Get items for 5th to last
d = l1[:5]      # Get items from 1st to 5th
e = l1[-2]      # Get the penultimate item
```

Une liste est modifiable. On peut donc en modifier ses éléments :

```python
l1[0] = 100     # Modify the 1st item
l1[2:4] = [50, 60, 70]  # Modify the 3rd, 4th and 5th items
```

Il est aussi possible d'ajouter de nouveaux éléments dans une liste existante.

```python
l1.append("important message")      # Add the item at the end
l1.insert(2, "220")     # Add the item between the 2nd and the 3rd
l2 = [6, 7, 8.6, "xyz"]
l1.extend(l2)           # Add the items of l2 list at the end of l1 list
```

Que signifie cette nouvelle syntaxe ; `variable_name.function()` ? Il s'agit en fait de la syntaxe pour appeler une *méthode* (une fonction d'un objet) d'instance.

Les méthodes `append()`, `insert()` et `extend()` vont "travailler" sur la liste référencée par `l1.

Les méthodes sont des fonctions, mais qui opèrent donc sur un objet, là où les fonctions comme `print()` ou `input()` s'utilisent "toutes seules" (et ne sont donc pas appelées méthodes). Nous expliquerons ce concept plus en détail dans la partie sur la POO.

Voici comment supprimer des éléments d'une liste :

```python
l1.remove("important message")  # Remove the item : "important message"
l1.pop(4)   # Remove the 5th item
```

Si vous voulez avoir plus d'information sur un type et ses méthodes, n'hésitez pas à aller voir la documentation en exécutant `help(list)` dans une console interactive.

Pour connaître le nombre d'éléments d'une liste, on utilise la fonction `len()` :

```python
l1_length =len(l1)
```

Pour savoir si un élément est présent dans une liste, on utilise le mot-clef `in` :

```python
6 in l1
```

Cette instruction renverra un booléen.

Voici comment copier une liste :

```python
l3 = l1.copy()
```

**NB** : la méthode `copy()` créer une nouvelle liste en mémoire, qui sera une copie de celle référencée `l1`. En revanche, si vous faites :

```python
l4 = l1
```

Vous ne créez pas une nouvelle liste, mais vous faites simplement référencer `l3` vers la même liste que `l1`. Les deux variables référenceront donc vers la même liste.

Vous pouvez vérifier vers quel objet référence une variable avec la fonction `id()` : exemple :

```python
print(id(l1))
print(id(l3))
print(id(l4))
```

Les variables `l1` et `l4` référencent bien vers le même objet, l'id renvoyé étant identique. En revanche `id(l3)` renvoie bien comme prévu un id différent.

Une liste est une séquence d'objets. Il est donc possible qu'une liste contienne des listes, et donc de créer une liste à plusieurs dimensions :

```python
matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
coef = matrix[0][2]
```

la variable `coef` vaut `3` (première ligne : indice 0, 3e colonne : indice 2).

### Le type `tuple`

Un tuple est une séquence ordonnée non modifiable (immuable ou *immutable* en anglais). C'est donc comme une liste, mais qui ne pourra plus être modifié après sa création.

Pour créer une liste, on utilise les parenthèses `()` :

```python
t = (1, 2, 3, 5, 6.5, "ok", False, "some text")
```

Pour récupérer un (ou des) élément(s) du tuple, on fait comme avec une liste :

```python
f = t[3]    # Get the 4th item of the tuple
```

Comme un tuple est immuable, on ne peut ni modifier des éléments du tuple, ni en ajouter ou en retirer.

Quel est l'intérêt d'utiliser un tuple, si l'on peut faire la même chose avec une liste ?

D'une part, créer une séquence "protégée". Dans certains cas, on peut souhaiter qu'une séquence reste constante. La stocker dans un tuple permet d'être sûr que celle-ci ne pourra pas être modifiée.

Le fait qu'un tuple soit immuable offre aussi un gain de performance. Si vous souhaitez effectuer beaucoup d'opérations à partir d'une séquence, utiliser un tuple permet d'avoir un programme plus rapide.

Néanmoins, pour des opérations mathématiques plus complexes, il vaudra mieux utiliser des librairies dédiées, comme `numpy` et `pandas`.

### Le type `range`

Le type `range` sert à créer une séquence de nombres entiers. Voici comment créer un `range` :

```python
r1 = range(10)      # Create a range from 0 to 10 excluded, with a step of 1
r2 = range(2, 10)   # Create a range from 2 to 10 excluded, with a step of 1
r3 = range(5, 50, 5)   # Create a range from 5 to 50 excluded, with a step of 5
```

La syntaxe `range()` appelle le **constructeur** de la classe `range`.

On peut en effet créer tout type d'objet de cette manière, par exemple :

```python
li = list((1, 2, 5, 10.5, "green"))     # Create a list
t = tuple((1, 4, 4.5, "ok", "ok"))      # Create a tuple
x = int(4)                              # Create a int
```

## La boucle `for`

En Python, la boucle `for` est utilisé pour parcourir des types composites, comme des listes, des tuples, des sets...

Écrivons par une boucle `for` qui ferait la même chose que la boucle `while` vu séance 2 :

```python
for n in range(10):
    print(f"n = {n}")
print("Program exited the for loop")
```

`n` va successivement prendre les valeurs allant de 0 à 9.

Autre exemple :

```python
languages = ["C", "C++", "Java", "Python", "HTML", "CSS", "PHP", "Go", "Rust"]
for language in languages:
    print(language)
```

Ici, la variable `langage` va prendre toutes les valeurs successives de la liste `langages`, soit `"C"`, puis `"C++"`... jusqu'à `"Rust"`

Il est aussi possible de récupérer en même temps l'élément et l'indice correspondant, grâce à la fonction `enumerate()` :

```python
for i, language in enumerate(languages):
    print(f"{i} : {language}")
```

Il est possible "d’altérer" une boucle avec deux mots clefs du langage : `break` et `continue`.

- `break` permet de sortir d'une boucle, sans terminer l'itération en cours.
- `continue` permet de terminer une itération en cours, sans exécuter les instructions qui suivent.

Exemple :

```python
for i in range(5, 500, 5):
    if i == 300:
        break
    if i % 10 == 0:
        continue
    print(i)
```

Si vous exécutez cette boucle, vous verrez que les multiples de 10 ne seront pas affichés dans la console. Car lorsque `i % 10 == 0`, l'instruction `continue` est exécutée, et donc l'instruction `print(i)` n'est pas exécuté.

De plus, le dernier nombre affiché sera `295`. Lorsque la condition `i == 300` est vraie, alors l'instruction `break` est exécutée, ce qui fait directement sortir le programme de la boucle `for`, sans passer par le `print(i)`

## Exercices

### Suite de Fibonacci

Écrivez un programme qui stockera dans une liste les 30 premiers nombres de la suite de Fibonacci.

Une fois la suite calculée, affichez là dans la console.

### Trier des légumes

Vous cultivez deux légumes différents : des choux et des patates. Vous possédez dans votre entrepôt deux réserves pour ranger les légumes séparément. Ces réserves sont symbolisées par deux listes :

```python
cabbages_stock = ["cabbage", "potato", "potato", "cabbage", "cabbage", "potato"]
potatoes_stock = ["cabbage", "cabbage", "cabbage", "potato", "potato", "cabbage", "potato", "cabbage"]
```

La suite de votre programme doit trier les légumes, pour que vos listes soient ainsi :

```
["cabbage", "cabbage", "cabbage", "cabbage", "cabbage", "cabbage", "cabbage", "cabbage]
["potato", "potato", "potato", "potato", "potato", "potato"]
```

Essayez de faire cet exercice de deux façons différentes. D'abord, en utilisant des boucles `for`, puis en essayant de trouver des méthodes du type `list` (en lisant la doc Python) permettant de faire les choses "plus rapidement".

### Jeu du pendu

Écrivez un jeu du pendu :

- Le programme demande d'écrire le mot à faire deviner
- Le programme demande le nombre d'erreurs maximum possibles
- Le jeu démarre et demande une proposition de lettre

Pour faciliter les choses, plutôt que de dessiner un vrai pendu, faites à la place une barre qui se remplit à chaque mauvaise proposition :

```
[xxxxx_____]
```
