# List creation
l1 = [1, 2, 3.5, "hello", True, "red", 3.5, 5.5445, "b"]

a = l1[0]       # Get the first item of the list
b = l1[2:7]     # Get items from 3rd to 7th
c = l1[4:]      # Get items for 5th to last
d = l1[:5]      # Get items from 1st to 5th
e = l1[-2]      # Get the penultimate item

l1[0] = 100     # Modify the 1st item
l1[2:4] = [50, 60, 70]  # Modify the 3rd, 4th and 5th items

l1.append("important message")      # Add the item at the end
l1.insert(2, "220")     # Add the item between the 2nd and the 3rd
l2 = [6, 7, 8.6, "xyz"]
l1.extend(l2)           # Add the items of l2 list at the end of l1 list

l1.remove("important message")  # Remove the item : "important message"
l1.pop(4)   # Remove the 5th item

l1_length = len(l1)

l3 = l1.copy()
l4 = l1

print(id(l1))
print(id(l3))
print(id(l4))

matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
coef = matrix[0][2]

# Tuple creation
t = (1, 2, 3, 5, 6.5, "ok", False, "some text")
f = t[3]    # Get the 4th item of the tuple

# Range creation
r1 = range(10)      # Create a range from 0 to 10 excluded, with a step of 1
r2 = range(2, 10)   # Create a range from 2 to 10 excluded, with a step of 1
r3 = range(5, 50, 5)   # Create a range from 5 to 50 excluded, with a step of 5

# Constructor syntax
li = list((1, 2, 5, 10.5, "green"))     # Create a list
t = tuple((1, 4, 4.5, "ok", "ok"))      # Create a tuple
x = int(4)                              # Create a int
