class Trouser:

    def __init__(self, color, material):
        self.color = color
        self.material = material
        self.zip_opened = False

    def __str__(self):
        return f"The trouser is {self.color} and is made of {self.material}. " \
               f"Is its zip opened : {self.zip_opened}"

    def open_zip(self):
        print("The Trouser is being opened")
        self.zip_opened = True

    def close_zip(self):
        self.zip_opened = False

class Jacket:

    def __init__(self, color, material):
        self.color = color
        self.material = material
        self.zip_opened = False

    def __str__(self):
        return f"The jacket is {self.color} and is made of {self.material}. " \
               f"Is its zip opened : {self.zip_opened}"

    def open_zip(self):
        print("The Jacket is being opened")
        self.zip_opened = True

    def close_zip(self):
        self.zip_opened = False


def close_clothing(cloth):
    cloth.close_zip()


if __name__ == "__main__":

    langages = ["Python", "C++", "JAVA"]
    name = "Sylvain"

    print(len(langages))
    print(len(name))

    print("Hello " + "world !")
    print(5 + 5)

    j1 = Jacket("Brown", "Acrylics")
    t1 = Trouser("Black", "Denim")

    for clothing in (j1, t1):
        clothing.open_zip()

    print(j1.zip_opened)
    print(t1.zip_opened)

    close_clothing(j1)
    close_clothing(t1)
    print(j1.zip_opened)
    print(t1.zip_opened)

