try:
    s = "bonjour"
    x = s + 40
    print("Should not be printed anyway")
except:
    print("Exception detected !")

print("Exception has been handled, program can continue")

try:
    n = 50
    d = int(input("Enter the denominator value"))
    result = n / d
except ValueError as e:
    print(f"{e} : You haven't entered a number for the denominator")
except ZeroDivisionError as e:
    print(f"{e} : Division can't be done. Denominator has been set to 0")
else:
    print(f"result of the division is {result}")
finally:
    print("finally, it is the end of this of this course")
