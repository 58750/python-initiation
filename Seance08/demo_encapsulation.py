class Trouser:

    def __init__(self, material):
        self._color = 0x000000
        self.material = material
        self.zip_opened = False

    def __str__(self):
        return f"The trouser is {self._color} and is made of {self.material}. " \
               f"Is its zip opened : {self.zip_opened}"

    def get_color(self):
        return self._color

    def set_color(self, color):
        if color > 0xFFFFFF or color < 0:
            raise ValueError("The color entered is not a correct rgb value")
        self._color = color

    def open_zip(self):
        self.zip_opened = True

    def close_zip(self):
        self.zip_opened = False


class Vehicle:

    def __init__(self):
        self.__brand = "Generic"


class Car(Vehicle):

    def __init__(self, wheels_nb):
        super().__init__()
        self.__brand = "Audi"
        self.wheels_nb = wheels_nb

    #def set_brand(self, brand):
    #    self.__brand = brand    # __brand is private in Vehicle : error

    def get_brand(self):
        return self.__brand


if __name__ == "__main__":
    t1 = Trouser("Denim")
    #t1.set_color(-2)
    #t1._color = -2
    t1.set_color(0x0011AA)


    c1 = Car(4)

    c1.__brand = "Volvo"
    print(dir(c1))
    print(c1.__brand)
    #c1.set_brand("Volvo")
    #print(dir(c1))
    c1._Vehicle__brand = "Renaud"
    print(c1._Vehicle__brand)
    print(c1.get_brand())
    print(id(c1.__brand))
    print(id(c1._Car__brand))


