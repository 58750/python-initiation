# Séance 8

## Le polymorphisme

*Poly* : plusieurs. *Morphe* : forme. Le polymorphisme, cela signifie donc que quelque chose peut avoir plusieurs formes.

En Python, on parle de polymorphisme des fonctions/méthodes. Un même nom de fonction peut en effet se comporter différemment en fonction du contexte.

### Fonctions *Built-in*

Prenons l'exemple de la fonction `len()`, qui sert à renvoyer la longueur d'un objet :

```python
languages = ["Python", "C++", "JAVA"]
name = "Sylvain"

print(len(languages))
print(len(name))
```

Sortie console :

```
3
7
```

La fonction se comporte différemment en fonction du type de l'argument. Lorsqu'il s'agit d'une liste, la fonction renvoie le nombre d'éléments de celle-ci. Lorsqu'il s'agit d'une chaîne de caractères, la fonction renvoie le nombre de caractères.

### Opérateurs

Comme les fonctions, les opérateurs peuvent servir à faire des choses différentes. Par exemple :

```python
print("Hello " + "world !")
print(5 + 5)
```

Dans le premier cas, l'opérateur `+` sert à concaténer deux chaînes de caractères. Dans le second, il sert à additionner deux nombres.

### Function et objets

Comme en Python, tout est objet, une fonction peut recevoir en argument n'importe quel type d'objet.

Par conséquent, cette fonction peut appeler une méthode de cet objet sans se soucier du type de ce dernier. Exemple :

```python
class Trouser:

    def __init__(self, color, material):
        self.color = color
        self.material = material
        self.zip_opened = False

    def __str__(self):
        return f"The trouser is {self.color} and is made of {self.material}. " \
               f"Is its zip opened : {self.zip_opened}"

    def open_zip(self):
        print("The Trouser is being opened")
        self.zip_opened = True

    def close_zip(self):
        self.zip_opened = False

class Jacket:

    def __init__(self, color, material):
        self.color = color
        self.material = material
        self.zip_opened = False

    def __str__(self):
        return f"The jacket is {self.color} and is made of {self.material}. " \
               f"Is its zip opened : {self.zip_opened}"

    def open_zip(self):
        print("The jacket is being opened")
        self.zip_opened = True

    def close_zip(self):
        self.zip_opened = False

def close_clothing(cloth):
    cloth.close_zip()


j1 = Jacket("Brown", "Acrylics")
t1 = Trouser("Black", "Denim")

for clothing in (j1, t1):
    clothing.open_zip()

print(j1.zip_opened)
print(t1.zip_opened)

close_clothing(j1)
close_clothing(t1)
print(j1.zip_opened)
print(t1.zip_opened)
```

On obtient en sortie :

```
The Jacket is being opened
The Trouser is being opened
True
True
```

La boucle `for` permet de parcourir le tuple contenant la veste et le pantalon. La variable `clothing` référence donc d'abord vers un objet de type `Jacket`, puis vers un de type `Trouser`. Dans les deux cas, `clothing.open_zip()` permet d'appeler la méthode de la bonne classe, sans avoir à se soucier vers quel type particulier référence `clothing`. Il faut simplement être certain que tous les éléments du tuple sont des instances d'une classe qui possède une méthode `open_zip()`.

La fonction `close_clothing(cloth)` est appelée aussi bien pour ouvrir la veste `j1` que le pantalon `t1`, en appelant dans les deux cas la méthode `close_zip()` de la classe de l'objet. Dans `close_clothing(clothing)`, la variable `cloth` peut ainsi faire référence aussi bien à un objet de type `Trouser` que de type `Jacket`, et ainsi appeler la méthode de l'une ou l'autre classe.

### Redéfinition (*override*)

[Ce concept, déjà présenté dans la partie sur l'héritage](../Seance07/Seance07.md) consiste également en du polymorphisme. En effet, les méthodes redéfinies dans une classe fille ont le même nom que celles de la classe mère.

### Surcharge (*overload*) de méthode

Dans d'autres langages de programmation, il est possible d'avoir plusieurs méthodes avec le même nom. En Python, c'est impossible.

Mais il est possible d'obtenir un comportement similaire en utilisant les arguments par défaut pour les paramètres.

[Exemple de la séance 5](../Seance05/Seance05.md) :

```python
def power(x, y=2):
    return x**y
```

La fonction va se comporter différemment en fonction du nombre d'arguments reçus en paramètre. Cette technique peut être utile si l'on a l'habitude d'utiliser la surcharge de méthode dans d'autres langages comme le JAVA.

Pour aller plus loin : *packing* et *unpacking* d'arguments avec `*`, `*args` et `**kwargs`

## L'encapsulation

L'encapsulation est un concept fondamental de la POO, qui désigne le fait de regrouper les données avec des méthodes qui pourront lire et manipuler ces données. Concrètement, cela signifie masquer ces données, pour en contrôler la visibilité ou non en dehors du regroupement.

Contrairement à d'autres langages, l'encapsulation n'est pas "stricte" en Python.

### Membres protégés

Un membre protégé est un membre qui n'est accessible que dans la classe où il est déclaré et dans les classes filles de celle-ci.

Les membres protégés doivent commencer par un `_`.

Il ne s'agit que d'une convention. Rien n'empêche d'accéder à ces membres depuis n'importe où, l'interpréteur Python ne l'empêchera pas. L'idée, c'est que si vous voyez un attribut ou une méthode commençant par `_`, alors cela veut dire "Vous ne devriez pas utilisez ça en dehors de la classe".

On utilise souvent les membres protégés pour... protéger un attribut. Par exemple, pour qu'un nombre qui ne peut pas être négatif le soit.

Pour pouvoir travailler sur ces attributs, on va plutôt passer par des *getters* et des *setters* :

```python
class Trouser:

    def __init__(self, material):
        self._color = 0x000000
        self.material = material
        self.zip_opened = False

    def __str__(self):
        return f"The trouser is {self._color} and is made of {self.material}. " \
               f"Is its zip opened : {self.zip_opened}"

    def get_color(self):
        return self._color

    def set_color(self, color):
        if color > 0xFFFFFF or color < 0:
            raise ValueError("The color entered is not a correct rgb value")
        self._color = color

    def open_zip(self):
        self.zip_opened = True

    def close_zip(self):
        self.zip_opened = False

if __name__ == "__main__":
    t1 = Trouser("Denim")
    t1._color = -2  # Will be modified anyway
    t1.set_color(-2)
```

Ici, `_color` est maintenant un attribut protégé. Pour modifier sa valeur, on appelle son *setter*. Celui-ci permet de vérifier que le code entré est bien un code RGB correct.

Mais, comme le prouve la ligne au-dessus de `t1.set_color(-2)`, l'attribut protégé peut tout à fait être modifié directement.

NB : Les méthodes peuvent aussi être protégées.

### Membres privées

Un membre privé est un membre qui n'est accessible que dans la classe où il est déclaré. Pour déclarer un membre privé, on place un double underscore `__` devant son nom.

```python
class Vehicle:

    def __init__(self):
        self.__brand = "Generic"


class Car(Vehicle):

    def __init__(self, wheels_nb):
        super().__init__()
        self.wheels_nb = wheels_nb

    def set_brand(self, brand):
        self.__brand = brand    # __brand is private in Vehicle : error

    def get_brand(self):
        return self._Vehicle__brand


c1 = Car(4)
#c1.__brand = "Volvo"
#c1.set_brand("Volvo")
print(dir(c1))
c1._Vehicle__brand = "Renaud"
c1.get_brand()
```

Impossible d'accéder à l'attribut `__brand` aussi bien depuis la classe `Car` que depuis le programme principal. En effet, pour protéger d'avantage les attributs privés, Python change le nom de la variable vu de l'extérieur : `_ClasseName__privateAttribute`.

La méthode `dir(Class)` renvoie une liste de tous les membres d'une classe. Cela peut être utile lorsqu'on commence à travailler avec une classe que l'on ne connait pas encore.

NB : il ne faut pas confondre membre privée et méthodes spéciales, qui sont les méthodes avec un double underscore avant et après leurs noms. En général il s'agit des méthodes "de base" héritées de la classe `object` (comme `__init__` et `__str__`), même si rien n'empèche d'écrire ses propres méthodes spéciales dans une classe.

On comprend ainsi pourquoi l'encapsulation n'est pas "stricte" en Python. Là où, en Java par exemple, un membre private ne pourra pas être utilisé ailleurs, il est toujours possible de le faire en Python. La philosophie de langage consiste plutôt à dire que vous ne *devriez* pas utiliser ce membre, car vous risqueriez de "casser" le fonctionnement de la classe.

## Les exceptions

En informatique les erreurs sont monnaies courantes : mauvaise syntaxe, donnée manquante ou mauvaise... Dans tous les cas, une erreur interrompt le programme, car il n'a pas ou plus les informations nécessaires pour continuer.

En Python, on fait la distinction entre erreur de syntaxe (que vous avez vraisemblablement déjà croisé en faisant des exercices) et exception.

Une erreur de syntaxe est une erreur d'écriture de votre code Python : oubli de parenthèses, mauvaise indentation du code... Cela est lié à l'écriture du code en lui-même, mais pas à un "mauvais" fonctionnement.

Le reste des erreurs sont des exceptions. Par exemple :

```python
a = 50
result = a / 0
```

Ce code lancera une exception, car l'interpréteur ne peut pas diviser un nombre par zéro.

```python
s = "bonjour"
x = s + 40
```

Ce code lancera un autre type d'exception, car on ne peut pas "additionner" un type `string` avec un type `int`.

Pour l'instant, nous n'avons que recevoir des erreurs, nous indiquant que quelque chose n'est pas correct dans la conception de notre code. Mais les exceptions peuvent aussi être interceptés, de sorte que le programme agisse en conséquence afin de continuer à pouvoir fonctionner.

### `try except` : Interception (*catching*) des exceptions

Le mot clef `try` permet de délimiter un bloc de code dans lequel, si une exception est lancée, celle-ci sera détectée et interceptée. Le mot clef `except`, qui l'accompagne forcément, correspond au bloc de code qui sera exécuté si une exception a été lancée par du code contenu dans le bloc `try`

```python
try:
    s = "bonjour"
    x = s + 40
except:
    print("Exception detected !")

print("Exception has been handled, program can continue")
```

En exécutant ce script, aucun message d'erreur en rouge n'apparait dans la console. À la place, c'est le message du `print` contenu dans le `except` qui apparait. Le programme continue ainsi son exécution.

Évidemment, il n'est pas du tout recommandé d'intercepter toutes les exceptions comme ci-dessus. Cela revient à négliger toutes les erreurs. L'intérêt est de pouvoir détecter des exceptions spécifiques :

```python
try:
    n = 50
    d = int(input("Enter the denominator value"))
    result = n / d
except ValueError as e:
    print(f"{e} : You haven't entered a number for the denominator")
except ZeroDivisionError as e:
    print(f"{e} : Division can't be done. Denominator has been set to 0")
```

Ce code peut agir différemment en fonction des deux exceptions possibles : soit les caractères rentrés ne peuvent pas être convertis en un nombre, soit ce nombre vaut 0.

Notez que l'ordre a une importance : certaines exceptions plus spécifiques sont des enfants d'autres exceptions. Si une exception est interceptée par un `except` détectant sa classe mère, elle ne pourra pas être redétectée par l'`except` de sa propre classe. Il faut donc classer ses `except` du plus au moins spécifique.

N'hésitez pas à chercher sur internet ou dans la doc Python la hiérarchie des exceptions.

### `else` et `finally`

Le mot clef `else` permet de définir du code qui sera exécuté si aucune erreur n'est levée. Le mot clef `finally` permet de définir du code qui sera exécuté dans tous les cas.

```python
try:
    s = "bonjour"
    x = s + 40
except:
    print("Exception detected !")

print("Exception has been handled, program can continue")

try:
    n = 50
    d = int(input("Enter the denominator value"))
    result = n / d
except ValueError as e:
    print(f"{e} : You haven't entered a number for the denominator")
except ZeroDivisionError as e:
    print(f"{e} : Division can't be done. Denominator has been set to 0")
else:
    print(f"result of the division is {result}")
finally:
    print("finally, it is the end of this of this course")
```

### Lever ses propres exceptions

Il est possible de lever ses propres exceptions, permettant de définir des erreurs particulières et afficher un message spécifique.

Revenons sur l'exemple illustrant les membres protégés :

```python
class Trouser:

    def __init__(self, material):
        self._color = 0x000000
        self.material = material
        self.zip_opened = False

    def __str__(self):
        return f"The trouser is {self._color} and is made of {self.material}. " \
               f"Is its zip opened : {self.zip_opened}"

    def get_color(self):
        return self._color

    def set_color(self, color):
        if color > 0xFFFFFF or color < 0:
            raise ValueError("The color entered is not a correct rgb value")
        self._color = color

    def open_zip(self):
        self.zip_opened = True

    def close_zip(self):
        self.zip_opened = False

if __name__ == "__main__":
    t1 = Trouser("Denim")
    t1._color = -2  # Will be modified anyway
    t1.set_color(-2)
```

Dans la méthode `set_color`, la ligne `raise ValueError("The color entered is not a correct rgb value")` lève/lance (*raise* en anglais) une exception.

Lorsqu'une exception est levée, le programme sort instantanément du contexte courant (la méthode) pour remonter au contexte supérieur, et ainsi de suite jusqu'à "tomber" sur un `try except` pouvant potentiellement gérer cette exception. Si aucun bloc `except` ne peut la gérer, alors le programme s'arrête.

Il existe beaucoup d'exceptions *built-in* en Python. Et comme pour tout objet, vous pouvez créer les vôtres, plus spécifiques, grâce à l'héritage.

## Exercices

### RPG v2

Encapsulation des attributs : faites en sorte que les statistiques des personnages soit protégés, pour éviter de pouvoir attribuer n'importe quel valeur (par exemple 1000 points de vie, 100 de défense...). Mettez en place un système de *getters* et de *setters* à la place.

Gestion d'exceptions : Mettez en place un système d'exception qui assure le fait qu'un personnage ne puisse pas attaquer si celui-ci n'a plus de vie.


