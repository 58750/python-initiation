# Initiation à la programmation : langage Python

Un langage de programmation est, comme son nom l'indique, un langage permettant de "parler" avec une machine. Le terme machine peut désigner toutes sortes d'appareils "informatiques" : un ordinateur, un smartphone, un objet connecté...

Pour être exact, écrire un programme "donne des ordres" à une machine, que celle-ci devra ensuite exécuter.

Un ordinateur n'est pas "intelligent". Il ne fait qu'effectuer ce qu'un programme lui demande de faire.

Il existe de nombreux langages de programmation, ayant chacun leur domaine de prédilection et leurs particularités.

Parmi les plus utilisés, on peut citer le C/C++, le C#, le Java, le JavaScript, et donc, le Python.

Le contenu de ce cours est découpé en séances :

[Séance 1: introduction, types, variables, print, console Python, logique booléenne](Seance01/Seance01.md)

[Séance 2: structures if elif else, boucle while, input, débugueur](Seance02/Seance02.md)

[Séance 3: liste, tuple, range, boucle for](Seance03/Seance03.md)

[Seance 4: set, frozenset, dictionnaire](Seance04/Seance04.md)

[Séance 5: fonctions, modules, gestion de fichiers](Seance05/Seance05.md)

[Séance 6: POO classes](Seance06/Seance06.md)

[Séance 7: héritage](Seance07/Seance07.md)

[Séance 8: polymorphisme, encapsulation, exceptions](Seance08/Seance08.md)

[Séance 9: décorateurs, abstraction, membres de classe et statique](Seance09/Seance09.md)

[Séance 10: interface graphique (GUI) PySimpleGUI](Seance10/Seance10.md)

TODO : Séance 11 : Data Science : numpy, pandas et matplotlib
