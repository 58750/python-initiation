# Séance 1

## Python, qu'est-ce que c'est ?

- C'est un langage de programmation inventé en 1989/90, à l'origine pour faire du *scripting* (Un script est un programme plus ou moins court, utilisé pour réaliser automatiquement des tâches 'répétitives').

- C'est un langage sous licence *open-source*. Cela signifie qu'il est utilisable gratuitement, y compris à des fins commerciales, et que le "code source" du projet est consultable sur Internet.

- C'est un langage interprété. Cela signifie qu'à chaque nouvelle exécution d'un programme/script Python, l'interpréteur Python va exécuter les instructions écrites dans ledit script. C'est-à-dire les "traduire" une par une pour que l'ordinateur les comprenne et puisse faire ce qui est demandé par le code Python. On trouve "à l'inverse" les langages compilés (comme le C/C++) ou le compilateur va "analyser" l'intégralité d'un code source pour donner une version traduite de ce dernier, compréhensible par la machine. Voici une analogie pour résumer :
	- Langage interprété : vous discutez avec une personne qui parle une langue que vous ne comprenez pas. Vous êtes accompagné d'un·e interprète (le mot est d'ailleurs bien choisi) qui vous traduit ses paroles "en direct", phrase par phrase.
	- Langage compilé : vous discutez avec cette même personne. La personne note tout ce qu'elle veut vous dire d'un coup, dans son application Google Translate. Vous récupérez son téléphone, sur lequel vous pouvez lire la traduction de tout ce qu'elle voulait vous dire.
	
    Un langage interprété présente un avantage : sa portabilité. Cela signifie que le programme pourra fonctionner sur n'importe quelle machine, si tant est que l’interpréteur Python est installé sur celle-ci.
    
    Mais cela présente aussi un inconvénient : sa performance. Même si celle-ci s'est beaucoup améliorée au fil des années, un même programme sera plus performant en langage compilé (par exemple en C++) qu'en Python.

- C'est un langage orienté objet. La programmation orientée objet est un paradigme (une vision) de programmation. On utilise le terme objet pour désigner une entité qui représente quelque chose, un objet donc. Un objet peut généralement avoir des caractéristiques et des fonctionnalités. En Python, **tout est objet**.

Alors, pourquoi apprendre Python ? (Une partie) de la réponse dans cette brève vidéo : https://www.youtube.com/watch?v=X4KivZmTM-I

## Comment utiliser Python sur sa machine ?

Il faut tout d'abord installer Python sur sa machine. Si vous utilisez Windows, vous pouvez le télécharger sur le site officiel : https://www.python.org/downloads/

Installer ce logiciel, c'est installer l'interpréteur Python sur sa machine.

Si vous utilisez MacOS ou Linux, Python doit normalement déjà être installé sur votre machine.

Avec cette seule installation. Il est donc déjà possible d'exécuter du code source écrit en Python. Vous pouvez tout à fait écrire un code Python dans n'importe quel éditeur de texte, comme le bloc-notes de base de Windows. Mais cela présenterait vite ses limites.

Généralement, lorsque l'on souhaite développer (c'est-à-dire écrire du code, programmer), on utilise un environnement de développement (IDE en anglais). Il s'agit d'un logiciel dont le cœur sera bien sûr un éditeur de texte, mais qui possédera beaucoup d'autres fonctionnalités bien utiles pour être plus efficace et travailler plus confortablement.

Il existe plusieurs IDE, pour différents langages et ayant chacun des spécificités différentes. Dans le cadre de ce séminaire, nous utiliserons PyCharm. La version *Community* est gratuite (et *open-source*, comme Python). Lien pour télécharger : https://www.jetbrains.com/pycharm/download/


Une fois les deux logiciels installés, nous allons pouvoir commencer à écrire du code. Au premier lancement de PyCharm, ce dernier vous proposera (entre autres) de créer un nouveau projet. Un projet s'apparente simplement à un dossier dans votre ordinateur. Le logiciel vous demandera où vous souhaitez créer votre dossier (choisissez un emplacement qui vous convient sur votre machine), puis cliquez sur "créer", en laissant le reste par défaut.

À ce moment, l'éditeur de texte de PyCharm devrait s'afficher.

![Image](./pyCharmStartup.png "PyCharm et son éditeur de texte")

À gauche se trouve l'explorateur de projet. Pour l'instant, vous pouvez voir qu'il n'existe qu'un seul fichier Python dans votre projet/dossier. Il a été créé automatiquement par PyCharm et s'appelle *main.py* (*.py* étant l'extension des fichiers Python, comme *.doc* pour un fichier Word par exemple).

Au centre se trouve l'éditeur de texte. C'est là que l'on peut écrire dans ses fichiers Python.

En haut à droite, se trouve un bouton *play* en vert. Cliquez dessus. Le script *main.py* vient d'être exécutée ! Un nouvel espace est normalement apparu en bas de la fenêtre. Il s'agit de la console. Cela permet au programme exécuté d'afficher des infos, mais aussi à l'utilisateur·ice du programme d'en envoyer vers Python.

## Écrire un premier script

Pour l'instant, le code source du fichier *main.py* peut vous sembler un peu "cryptique". Nous verrons plus tard ce qu'il signifie. En attendant, vous pouvez créer un nouveau fichier Python. Pour cela, faites clic droit sur le dossier de projet en haut à gauche (appelé *pythonProject* sur la capture du dessus, mais peut-être avec un autre nom chez vous) et faites nouveau fichier → fichier Python. Nommez-le par exemple *demo1*.

Ce fichier est automatiquement ouvert dans l'éditeur. Vous pouvez voir qu'il y a maintenant deux onglets au-dessus de celui-ci. Vous pouvez ainsi avoir plusieurs fichiers ouverts en même temps dans votre éditeur et passer de l'un à l'autre aisément. Vous pouvez également constater, et l'on pouvait s'y attendre, que ce fichier est complètement vide.

Vous allez ainsi pouvoir écrire votre premier programme en python. Pour l'instant, ajoutez simplement en première ligne :

```python
print("hello world")
```

Faites clic droit sur l'onglet de *demo1.py* puis *lancer 'demo1'*

La console en bas de PyCharm affiche maintenant une première ligne (vous indiquant quel fichier/script vient d'être interprété) puis le message `hello world`. Enfin, en dessous se trouve une ligne vous indiquant que l'exécution du script s'est terminé.

Le script que vous venez d'écrire est très simple : il ne contient qu'une seule instruction. En Python, une instruction = une ligne.

`print()` est ce qu'on appelle une fonction. Nous aborderons ce concept plus en détail plus tard, mais vous pouvez déjà garder en tête que cela suit le même principe qu'une fonction en mathématiques. Néanmoins, on comprend qu'elle sert ici à afficher quelque chose dans la sortie console.

Ajoutez à la ligne 2 l'instruction suivante : 

```python
print(4+5) # After the hashtag symbol is a comment
```

Puis relancez le script. Vous verrez cette fois dans la sortie, en dessous de `hello world`, le chiffre `9`. Nous venons ici de réaliser une addition de deux nombres entiers, dont le résultat est affiché par `print()`

Ce qui est écrit à droite de la fonction `print()` est un commentaire. Tout ce qui est écrit après un `#` n'est pas "lu" par l'interpréteur, pour ce dernier, c'est comme si ce texte n'existait pas. On utilise les commentaires pour annoter son code.

Voici comment faire pour commenter plusieurs lignes :

```python
"""
print(4+5)
print(5+10)
print(10+50)
"""
```

Le symbole `+` est un opérateur (comme en mathématiques). En Python, comme dans la plupart des langages, nous pouvons utiliser des opérateurs pour faire des calculs mathématiques. En voici la liste :

| symbole | fonctionnalité   |
|---------|------------------|
| \+      | addition         |
| \-      | soustraction     |
| \*      | multiplication   |
| \**     | puissance        |
| \/      | division         |
| \//     | division entière |
| \%      | modulo           |

Effectuer des calculs ligne par ligne peut sembler intéressant, mais le vrai intérêt va réellement être de pouvoir stocker ces résultats, afin de pouvoir les réutiliser plus tard dans notre script.

## Les variables

C'est à ce moment que le concept de variable entre en jeu. Une variable sert à référencer une valeur, ou plus exactement, en Python, toujours un objet.

Rappelez-vous de ce qui a été dit plus tôt : en Python, **tout est objet**. Nous allons tenter d'expliquer un peu mieux ce que cela signifie.

Toujours dans le même fichier, ajoutez à la ligne suivante :

```python
x = 5
```

Ici, x est une variable. Ce que fait cette instruction, c'est affecter à la variable `x` la valeur `5`. Contrairement aux mathématiques, le symbole `=` ne marque pas une égalité entre ce qui se trouve à sa gauche et à sa droite, mais bien le fait de venir affecter ce qui se trouve à droite à la variable de gauche (à gauche du `=` se trouve toujours une variable).

**Convention de nommage** : En Python, on utilise le formatage *snake_case* : tous les mots en minuscule, en les séparant par un `_`. Exemple : `length`, `nb_values`.

Ci-dessous un lien reprenant toutes les conventions de nommage du langage : 
https://pythonguides.com/python-naming-conventions/

Essayez de toujours utiliser l'anglais dans votre code, y compris dans vos commentaires. En particulier, évitez les symboles autres qu'alphanumériques (pas d'accents).

Vous pouvez maintenant ajouter les lignes suivantes : 

```python
y = 10
z = x + y
print(z)
```

Si vous lancez le script, vous verrez à la suite des autres prints le nombre entier `15`. En effet, nous avons bien affecté à la variable z la somme de `x` et de `y`, à savoir `5 + 10`.

Mais alors, qu'est-ce qu'un objet, et qu'est-ce qu'une variable par rapport à un objet ? Quand on écrit ` y = 10`, `y` est une variable, et `10` un objet de **type** (nous expliquerons ce terme juste après) `int` pour *integer* en anglais, soit un nombre entier.

Ce n'est pas `y` qui vaut directement `10`. En fait, la variable sert de référence vers un objet de type `int` qui "contient" le nombre 10.

Prenons l'exemple d'une bibliothèque universitaire. Si je veux trouver un livre, j'ai besoin de sa référence, ou plus exactement de sa côte de rangement (par exemple : BIB. : P. du Midi 53 EX/COR KAN). Cette côte, c'est l'équivalent de notre variable. Elle me permet de savoir où se trouve le livre en question, mais **N'EST PAS** le livre en question. Le livre, c'est l'équivalent de notre objet.

Pour filer davantage la métaphore, les étagères de la bibliothèque sont l'équivalent de la mémoire vive (la RAM) de notre machine. Quand un nouvel objet est créé, celui-ci est stocké dans cette mémoire vive. Et c'est la variable qui nous permet de savoir où est rangé cet objet dans la mémoire.

Maintenant, ajoutez la ligne suivante :

```python
print(4		# Will generate an error
```

Puis relancez votre programme. Vous devriez obtenir un affichage similaire dans la console : 

```
  File "/home/sylvain/HEFF/2021-2022/PYTHON/seminaire-prog-python/code/demos/demo1.py", line 11
    str1 = "hello world "   # objet String : chaine de caractères
    ^
SyntaxError: invalid syntax
```

Vous venez d'effectuer votre première erreur ! En effet, la parenthèse de la fonction `print()` n'a pas été fermé.

Vous pouvez constater que l'éditeur souligne en rouge une partie de votre code, car PyCharm a détecté qu'il y avait une erreur de syntaxe. Cette fonctionnalité fait partie des nombreux avantages d'un IDE par rapport à un éditeur de texte type bloc-notes Windows. D'ailleurs, vous avez peut-être déjà remarqué que quand vous ouvrez des parenthèses, Le symbole "`)`" est ajouté automatiquement. Une autre fonctionnalité bien pratique.

## Les types natifs

En python, chaque objet a un type. C'est-à-dire ce qu'il \*est\*. Il est tout à fait possible (c'est le but de la programmation orientée objet) de créer ses propres types. Mais Python fournit déjà des types dits natifs (*builtin* en anglais), qui sont utilisables directement dans n'importe quel script.

Nous avons déjà parlé du type `int`, qui représente un nombre entier. Voici quelques un des types natifs :

| Dénomination en Python | en français |
|-| -|
| int | nombre entier optimisé |
| float | nombre à virgule flottante |
| complex | nombre complexe |
| bool | booléen |
| str | chaîne de caractère |
| range | série de nombres |
|list | liste ordonnée de longueur variable |
|tuple | liste ordonnée de longueur fixe |
| set | liste non ordonnée de longueur variable |
|frozenset | liste non ordonnée de longueur fixe |
| dict | dictionnaire : liste non ordonnée de paires *key* : *value* |
| bytes |liste ordonnée d'octets de longueur fixe |
| bytearray | liste ordonnée d'octets de longueur variable |
| memoryview | |
| file | fichier |
| NoneType | absence de type |
| NotImplementedType | absence d'implémentation |
| function | fonction |
|module | module |

Certains de ces types seront abordés plus tard. Commençons par présenter les types "de base".

`int` : nombre entier. Rien de compliqué ici, représente un nombre entier, positif ou négatif.

`float` : nombre à virgule flottante. Idem, pas besoin de savoir ce que signifie flottante pour les utiliser. À noter qu'en Python (et dans la plupart des langages), on représente la virgule par un point : `a = 4.52`

`bool` : un booléen ne peut prendre que deux valeurs : `True` ou `False`. Nous verrons à quoi ce type sert un peu plus tard.

`str` : *String*, chaine de caractère. À vrai dire, nous avons déjà utilisé une chaine de caractère :

```python
str1 = "hello world"
```

Ici, `"hello world"` en est une. Pour écrire une chaîne de caractère, on note celle ci entre guillemets. On peut aussi utiliser des apostrophes ou même des triples guillemets.

```python
str1 = "hello"
str2 = "hello \"friends"
str3 = """I hope you are doing "well"\n me, yes"""
print(str2 + str3)
```

Deux choses à noter : il existe des caractères "spéciaux", que l'on peut écrire dans une chaîne à l'aide du `\` (antislash ou *backslash* en anglais). Par exemple, `\n` est un caractère de saut de ligne. Un autre caractère souvent utilisé est `\t`, pour insérer une tabulation.

Dans la troisième ligne, nous utilisons le symbole `+` pour afficher les deux chaînes de caractères à la suite. On appelle cela une concaténation.

Une chaîne de caractère est une **séquence**. Cela signifie que l'on peut voir celle-ci comme une "suite" de caractères simples. Essayez d'exécuter ces instructions dans votre script :

```python
str1 = "hello world"
first_char = str1[0]
third_char = str1[2]
last_char = str1[-1]
print(first_char)
print(third_char)
print(last_char)
```

Sera alors affiché dans la console : 

```
h
l
d
```

Les instructions lignes 2, 3 et 4 viennent à chaque fois affecter un des caractères de la chaîne dans 3 variables différentes.

On appelle indice (souvent appelé `i`) l'entier placé entre crochets, permettant de récupérer le i-ième élément de la chaîne.

**IMPORTANT : en informatique, on commence toujours à compter à partir de 0.**

Utiliser un indice négatif permet de démarrer depuis la fin de la chaîne. (-2 renverra l'avant-dernier caractère, et ainsi de suite).

On dit que Python qu'il est un langage dynamique. Étudions ce bout de code :

```python
a = 5
a = 8.6
a = "Sylvain"
```

Ici, nous n'avons créé qu'une seule variable. On lui affecte d'abord un `int`, puis un `float` et enfin un `str`. Si vous ajoutez ce code à votre script et que vous l'exécutez, tout va fonctionner correctement. Cette caractéristique rend Python assez flexible d'utilisation. Dans d'autres langages, il faut spécifier explicitement le type d'une variable, et il serait donc impossible pour celle-ci de successivement faire référence à un entier puis à un *string*.

Le code ci-dessous présente quelques fonctions de conversion de type, qui nous seront utiles plus tard.

```python
nb = 5
nb_str = str(nb)
txt1 = "4"
txt1_int = int(txt1)
txt2 = "5.23"
txt2_float = float(txt2)
```

`nb` est de type `int`. La ligne en dessous de cette affection sert à convertir cet entier en une chaîne de caractère, qui sera `"5"`. Cette conversion s'effectue grâce à la fonction `str()`.

Inversement, il est possible de convertir une chaîne de composée de symboles représentant un nombre (entier ou à virgule) en un type `int` ou `float`. Pour cela, on utilise les fonctions `int()` et `float()`.

On dit que ces trois fonctions "renvoient" un objet. En effet, celles-ci sont placées à gauche de l'opérateur d'affectation (le `=`). `nb`, `a` et `b` vont donc respectivement référencer vers un objet de type `str`, `int` et `float`.

Vous souhaitez connaître le type d'un objet ? Utilisez la fonction `type()` :

```python
print(type(nb))
print(type(txt1_int))
print(type(txt2_float))
```

Convertir un nombre en un type `str` peut être utile, et, pour l'instant, peut vous sembler la seule solution pour afficher un nombre et des *string* dans un même `print()`. Mais pour rendre son code plus lisible, on utilise le formatage de *string*. Prenons un exemple :

```python
english_mark = 18
math_mark = 17.5
```

On souhaite afficher dans la console le message suivant :

```
You got 18 in english and 17.5 in maths.
```

Avec la concaténation via l'opérateur `+`, on ferait comme ça :

```python
print("You got " + str(english_mark)
      + " in english " + str(math_mark) + "  in maths.")
```

Ce qui est peu lisible. Voilà comment utiliser les *string* formatés :

```python
print(f"You got {english_mark} in english and {math_mark} in maths.")
```

Le `f` devant les guillemets permettent de formater ça chaîne et ainsi la rendre plus compréhensible dans votre code.

## La console interactive

Nous avons expliqué précédemment que Python est un langage interprété. À chaque fois que vous lancez un script, l'interpréteur va traduire "ligne par ligne" le code source pour que l'instruction puisse être comprise et exécutée par la machine.

Il est donc tout à fait possible de tester du code en direct. Pour cela, on utilise ce qu'on appelle parfois la console interactive.

Il y a plusieurs moyens d'accéder à une console interactive. Si vous êtes sous Windows, vous pouvez lancer le programme "Python 3.x" (x devrait normalement être 10 chez vous, si vous avez installé la dernière version). Une fenêtre noire devrait s'ouvrir, dans laquelle vous pouvez écrire.

Il s'agit de la console "de base" de Python, qui est présente sur votre machine parce que Python est installé.

Écrivez l'instruction suivante, puis appuyez sur Entrée :

```python
a = 5
```

En faisant ceci, vous venez d'envoyer à l'interpréteur une instruction. À la ligne suivante, écrivez cette instruction :

```python
print(a)
```

La console devrait vous afficher "5" à la ligne du dessous ! Il est donc possible de tester des petites portions de code rapidement de cette manière. Cela permet d'éviter de lancer à chaque fois votre script en entier, si vous êtes en train de travailler sur un point spécifique de votre programme.

Il est également possible d'utiliser une console interactive à l'intérieur de PyCharm. Tout en bas de la fenêtre se trouve plusieurs onglets. Celui tout à droite, "Console Python" permet d'ouvrir la console interactive.

L'avantage de cette console par rapport à celle de base, c'est que l'on peut voir à tout moment, dans la partie droite, quelles sont les variables déclarées, vers quel type d'objet elles référencent, et ce que contient l'objet en lui-même.

## La logique booléenne

Nous avons évoqué précédemment le type `bool`, qui peut prendre deux valeurs : `True` ou `False`. On peut ainsi faire référencer une variable vers un booléen

```python
a, b = True, False
```

NB : cette instruction est une affectation parallèle : `a` vaudra `True`, et `b` vaudra `False`

Utiliser une variable pouvant être `True` ou `False` peut présenter un intérêt. Mais l'intérêt principal des booléens est de pouvoir indiquer le résultat d'une **comparaison**, qui s'effectuent à l'aide de comparateurs :

| symbole | signification |
| - | - |
| `<` | strictement inférieur |
| `>` | strictement supérieur |
| `<=` | inférieur ou égal |
| `>=` | supérieur ou égal |
| `==` | égal |
| `!=` | différent |
| `x is y` | x et y représentent le même objet |
| `x is not y` | x et y ne représentent pas le même objet |

Testez maintenant ces lignes de code dans votre script :

```python
a = 5 < 6
print(a)
```

La console affiche `True`

Les opérateurs logiques se retrouvent souvent utilisés avec les opérateurs logiques :

| symbole | signification |
| - | - |
| `x or y` | OU booléen|
| `x and y` | ET booléen |
| `not x` | NON booléen |

Ajoutez les lignes suivantes à votre script : 

```python
a = 5 < 6 and 10 < 6
print(a)
```

Cela va afficher `False`. En effet, on comprend intuitivement que ET signifie que les deux *conditions* (celle à gauche et celle à droite du `and`) doivent être vraies.

Si vous remplacez le `and` par un `or`, alors le `print()` affichera cette fois `True`. Car il suffit qu'une des deux conditions soient vraies pour que l'ensemble renvoie `True`.

## Exercices

### Calcul d'une moyenne

Ouvrez le fichier `exo1_1.py` Il contient 4 instructions :

```python
mark1 = 16
mark2 = 4
mark3 = 9
mark4 = 14
```

Complétez ce script pour afficher la moyenne de ces 4 valeurs dans la console. Il faudra passer par la création d'une variable `m` (moyenne en anglais).

### Calculer la somme de 3 nombres

Ouvrez le fichier `exo1_2.py`. Il contient les lignes suivantes :

```python
a = 12.5
b = 5
c = "4.25"
```

Complétez ce script pour afficher la somme des 3 nombres : 12.5, 5 et 4.25. Il faudra passer par la création d'une variable s (pour *sum* en anglais).

### Détection d'un multiple de 3

Ouvrez le fichier `exo1_3.py`. Il contient les lignes suivantes : 

```python
a = 5
```

Complétez ce script pour qu'il affiche `True` si a est multiple de 3, et `False` si ce n'est pas le cas. Changez la valeur de `a` pour tester si votre script fonctionne correctement. Il faudra passer par une variable `b` pour référencer vers le booléen.