class Vehicle:

    def __init__(self, brand):
        self.brand = brand
        self.started = False

    def __str__(self):
        return f"The vehicle is a {self.brand}"

    def start(self):
        self.started = True

    def stop(self):
        self.started = False


class Car(Vehicle):

    def __init__(self, brand, wheels_nb):
        super().__init__(brand)
        self.wheels_nb = wheels_nb

    def __str__(self):
        return f"The car is a {self.brand} and has {self.wheels_nb} wheels"


class Convertible(Car):

    def __init__(self, brand, wheels_nb):
        super().__init__(brand, wheels_nb)
        self.opened_sunroof = False

    def __str__(self):
        return f"The convertible is a {self.brand}, has {self.wheels_nb} wheels"

    def open_sunroof(self):
        self.opened_sunroof = True

    def close_sunroof(self):
        self.opened_sunroof = False


class Housing:

    def __init__(self, floors_nb, bedrooms_nb):
        self.floors_nb = floors_nb
        self.bedrooms_nb = bedrooms_nb

    def __str__(self):
        return f"The housing has {self.floors_nb} with {self.bedrooms_nb} bedroom(s)"


class CampingCar(Car, Housing):

    def __init__(self, brand, wheels_nb, bedrooms_nb):
        Housing.__init__(self, 1, bedrooms_nb)
        Car.__init__(self, brand, wheels_nb)

    def __str__(self):
        return f"The camping car is made by {self.brand} and has {self.bedrooms_nb} bedrooms"


if __name__ == "__main__":
    v = Vehicle("Suzuki")
    c = Car("Ford", 4)

    print(v)
    print(c)

    v.start()
    print(v.started)

    c.start()
    # Car.start(c)    # do the same thing as line above
    print(c.started)
    c.stop()
    print(c.started)

    conv = Convertible("Renaud", 4)
    conv.start()
    conv.open_sunroof()

    camp_car = CampingCar("Rapido", 8, 2)
    print(camp_car)

    print(isinstance(camp_car, CampingCar))
    print(issubclass(Convertible, Vehicle))



