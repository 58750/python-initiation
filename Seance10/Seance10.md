# Séance 10

## Les interfaces graphiques : PySimple GUI

TODO : explications + exemples de base.

Mais en attendant, tout ce trouve dans la doc officielle :

[PySimpleGUI User's Manual : documentation avec tutos/cours pas à pas (en anglais)](https://pysimplegui.readthedocs.io/en/latest/)
[PySimpleGUI Interactive eCookbook : beaucoup d'exemples de code, testables directement dans le navigateur](https://pysimplegui.trinket.io/demo-programs#/demo-programs/intro-to-this-page)

