s = {1, 4, 3, 4, 5, 5, "Hello", 4.5}

print("Hello" in s)     # Will print True if "Hello" is in s

s.add("red")        # Add one item
s.update([93, 394.42, "green", 4.5])    # Add a sequence

s.remove(1)     # Will raise an error if item is not in the set
s.discard(3)
s.discard(10)   # Will NOT raise an error even if the item is not in the set
last = s.pop()  # Remove "last" item and return it
s.clear()       # Remove every items

s1 = {1, 4, 69, "red", 9.332, 93, "yellow"}
s2 = {93, 4, 4.32, "blue", "yellow"}
su = s1.union(s2)       # Returns a new set containing elements of s1 and s2
si = s1.intersection(s2)    # Returns the intersection of s1 and s2
sd = s1.symmetric_difference(s2)    # Return the symmetric difference : every elements present in only 1 set

fs = frozenset([1, 2, 2, 4.5, "black", "grey", 5.5])
