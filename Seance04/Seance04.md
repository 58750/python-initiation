# Séance 4

## Les *sets*

Les sets sont des types composites (*collection*) permettant de regrouper des données de manière non ordonnée (là où les listes et les tuples sont des types ordonnés).

### Le type `set`

Un set est une séquence non ordonnée de longueur variable. On utilise les accolades `{}` pour créer un set :

```python
s = {1, 2, 3, 4, 5, 5, "Hello", 4.5}
```

Comme il s'agit d'une séquence non ordonnée, ses éléments ne sont pas indexables. La syntaxe `s[i]` "n'existe pas" pour des sets.

Contrairement aux listes ou aux tuples, un set ne peux pas contenir deux fois un même élément. Dans l'exemple ci-dessus, vous pourrez voir que `s`ne contient qu'une seule fois l'entier `5`, alors qu'il apparait deux fois entre les accolades `{}`.

Pour savoir si un élément est présent dans un set, on utilise le mot clef `in`.

```python
print("Hello" in s)     # Will print True if "Hello" is in s
```

Les éléments d'un set ne sont pas modifiables. Mais on peut ajouter ou enlever des éléments.

Ajout d'un ou plusieurs éléments :

```python
s.add("red")        # Add one element
s.update([93, 394.42, "green", 4.5])    # Add a sequence
```

Retrait :

```python
s.remove(1)     # Will raise an error if item is not in the set
s.discard(3)
s.discard(10)   # Will NOT raise an error even if the item is not in the set
last = s.pop()  # Remove last item
s.clear()       # Remove "last" item and return it
```

**ATTENTION** : `pop()` retire le "dernier" élément du set. Mais comme un set est non ordonné, ce "dernier élément" n'est pas forcément le dernier ajouté.

Il est possible de créer des jonctions de plusieurs sets.

```python
s1 = {1, 4, 69, "red", 9.332, 93, "yellow"}
s2 = {93, 4, 4.32, "blue", "yellow"}
su = s1.union(s2)       # Returns a new set containing elements of s1 and s2
si = s1.intersection(s2)    # Returns the intersection of s1 and s2
sd = s1.symmetric_difference(s2)    # Return the symmetric difference : every element present in only 1 set
```

- Union : renvoie un set avec tous les éléments des deux sets
- Intersection : renvoie un set avec les éléments contenus dans les deux sets
- Différence symétrique : renvoie un set avec les éléments contenus dans un seul des deux sets

La méthode `union()` est un équivalent de la méthode `update()`, mais qui créé un nouveau set au lieu de mettre a jour un set déjà existant.
`intersection_update()` et `symmetric_difference_update()` permettent de faire la même chose.

### Le type `frozenset`

Le type `frozenset` désigne un set de longueur fixe. Ce qui signifie que, contrairement à un set, on ne peut ni ajouter ni enlever des éléments. Exemple :

```python
fs = frozenset([1, 2, 2, 4.5, "black", "grey", 5.5])
```

## Le type `dict`

Un dictionnaire est un type composite.

Pour en expliquer son principe, il suffit de faire l'analogie avec un dictionnaire. Un dictionnaire est une liste de mot, possédant chacun une définition. En Python, chaque clef (*key* en anglais) du dictionnaire "possède" une valeur (*value* en anglais).

Comme un vrai dictionnaire, un dictionnaire Python ne peux pas contenir deux clefs identiques.

On peut donc voir un dictionnaire Python comme un ensemble de paires d'éléments clefs : valeurs.

Création d'un dictionnaire :

```python
d = {
    6: 4.5,
    "size": [4, "flat"],
    75.23: False,
    (1, 2, 3): 10,
    }
```

NB : ici, l'instruction de création du dictionnaire est écrite sur plusieurs lignes. Cela permet de rendre son code plus lisible. On peut bien sur utiliser ce formatage pour des listes, tuples...

N'importe quel objet peut être placé dans un dictionnaire. En revanche, la clef doit être un objet *hashable*. Pour faire simple, retenez qu'un objet *hashable* est immuable. Donc, impossible par exemple d'avoir une liste en tant que clef.

En pratique, on utilise très souvent des `str` pour les clefs.

Voici comment récupérer ou modifier la valeur d'une clef :

```python
a = d[6]                # Gets value of the key 6
d[75.23] = 1            # Modify value of the key 75.23
d.update({75.23: 10})   # Modify a value of a key via update() method
```

Voici comment ajouter une paire clef : valeur :

```python
d["hello"] = 55     # Create a new "hello" key and assigns it the value 55
d.update({"color": "red"})  # Create a new "color" key and assigns it the value "red"
```

Voici comment supprimer des éléments :

```python
d.pop("hello")  # Delete the item (key and value)
d.popitem()     # Delete the last added item
del d[(1, 2, 3)]   # Delete via del Python keyword
d.clear()       # Delete all items
```

`del` est un autre mot clef du langage Python. Il sert à supprimer un objet. L'instruction ci-dessus supprime donc la liste `[4, "hello"]`. Si on supprime la valeur d'une clef, la clef est également retirée du dictionnaire.

NB : on peut utiliser `del` pour supprimer n'importe quel objet.

Comme pour les listes, il existe plusieurs méthodes pour travailler avec des dictionnaires. En voici quelques-unes :

- `keys()` : renvoie les clefs d'un dictionnaire
- `values()` : renvoie les valeurs
- `items()` : renvoie les clefs et les valeurs, sous forme de tuples

Ces méthodes sont utiles pour parcourir un dictionnaire via une boucle `for` :

```python
for key, value in d.items():
    print(f"{key} : {value}")
```

N'hésitez pas à aller lire la doc si vous souhaitez découvrir d'autres fonctionnalités.

## Exercices

### 

### <a name="gdcv1"></a> Gestionnaire de comptes

Écrivez un programme de gestion de comptes, C'est-à-dire des paires `username`; `password`. Ces comptes devront être stockés dans un dictionnaire.

Le programme doit intégrer trois fonctionnalités :

- Création d'un nouveau compte
- Changement du mot de passe d'un compte
- Suppression d'un compte
- Affichage des pseudos existants

Comme pour un vrai site :

- il ne peut pas y avoir plusieurs comptes avec le même *username*
- Le programme doit demander le mot de passe et vérifier qu'il soit correct avant de le modifier ou de supprimer le compte

Le programme devra tourner dans une boucle infinie : `while True:`. Code de départ :

```python
while True:
    choice = input('Select an action :\n1 : create an account\n2 : delete an account'
                   '\n3 : change a password\nq : quit')
    if choice == "q":
        quit()
    elif choice == 1:
        # TODO
        pass
    # TODO
```

NB : la fonction `quit()` permet de quitter directement le programme.
